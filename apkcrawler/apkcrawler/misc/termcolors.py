#!/usr/bin/python
#-*- coding:utf-8 -*-
__author__ = 'allen chen'
if 64 - 64: i11iIiiIii
if 65 - 65: O0 / iIii1I11I1II1 % OoooooooOO - i1IIi
if 73 - 73: II111iiii
color_names = ( 'black' , 'red' , 'green' , 'yellow' , 'blue' , 'magenta' , 'cyan' , 'white' )
foreground = dict ( [ ( color_names [ x ] , '3%s' % x ) for x in range ( 8 ) ] )
background = dict ( [ ( color_names [ x ] , '4%s' % x ) for x in range ( 8 ) ] )
if 22 - 22: I1IiiI * Oo0Ooo / OoO0O00 . OoOoOO00 . o0oOOo0O0Ooo / I1ii11iIi11i
RESET = '0'
opt_dict = { 'bold' : '1' , 'underscore' : '4' , 'blink' : '5' , 'reverse' : '7' , 'conceal' : '8' }
if 48 - 48: oO0o / OOooOOo / I11i / Ii1I
if 48 - 48: iII111i % IiII + I1Ii111 / ooOoO0o * Ii1I
def colorize ( text = '' , opts = ( ) , ** kwargs ) :
 i1I1ii1II1iII = [ ]
 if 86 - 86: ooOoO0o
 if 9 - 9: OOooOOo - i1IIi
 if 12 - 12: I1ii11iIi11i / OoO0O00 - o0oOOo0O0Ooo - i1IIi * i1IIi % Oo0Ooo
 if 68 - 68: Ii1I / O0
 if 46 - 46: O0 * II111iiii / IiII * Oo0Ooo * iII111i . I11i
 if 62 - 62: i11iIiiIii - II111iiii % I1Ii111 - iIii1I11I1II1 . I1ii11iIi11i . II111iiii
 if 61 - 61: oO0o / OoOoOO00 / iII111i * OoO0O00 . II111iiii
 if 1 - 1: II111iiii - I1ii11iIi11i % i11iIiiIii + IiII . I1Ii111
 if 55 - 55: iIii1I11I1II1 - I1IiiI . Ii1I * IiII * i1IIi / iIii1I11I1II1
 if 79 - 79: oO0o + I1Ii111 . ooOoO0o * IiII % I11i . I1IiiI
 if 94 - 94: iII111i * Ii1I / IiII . i1IIi * iII111i
 if 47 - 47: i1IIi % i11iIiiIii
 if 20 - 20: ooOoO0o * II111iiii
 if 65 - 65: o0oOOo0O0Ooo * iIii1I11I1II1 * ooOoO0o
 if 18 - 18: iIii1I11I1II1 / I11i + oO0o / Oo0Ooo - II111iiii - I11i
 if 1 - 1: I11i - OOooOOo % O0 + I1IiiI - iII111i / I11i
 if 31 - 31: OoO0O00 + II111iiii
 if 13 - 13: OOooOOo * oO0o * I1IiiI
 if 55 - 55: II111iiii
 if 43 - 43: OoOoOO00 - i1IIi + I1Ii111 + Ii1I
 if 17 - 17: o0oOOo0O0Ooo
 if 64 - 64: Ii1I % i1IIi % OoooooooOO
 if 3 - 3: iII111i + O0
 if 42 - 42: OOooOOo / i1IIi + i11iIiiIii - Ii1I
 if 78 - 78: OoO0O00
 if 18 - 18: O0 - iII111i / iII111i + ooOoO0o % ooOoO0o - IiII
 if 62 - 62: iII111i - IiII - OoOoOO00 % i1IIi / oO0o
 if 77 - 77: II111iiii - II111iiii . I1IiiI / o0oOOo0O0Ooo
 if text == '' and len ( opts ) == 1 and opts [ 0 ] == 'reset' :
  return '\x1b[%sm' % RESET
 for i1iIIIiI1I , OOoO000O0OO in kwargs . iteritems ( ) :
  if i1iIIIiI1I == 'fg' :
   i1I1ii1II1iII . append ( foreground [ OOoO000O0OO ] )
  elif i1iIIIiI1I == 'bg' :
   i1I1ii1II1iII . append ( background [ OOoO000O0OO ] )
 for iiI1IiI in opts :
  if iiI1IiI in opt_dict :
   i1I1ii1II1iII . append ( opt_dict [ iiI1IiI ] )
 if 'noreset' not in opts :
  text = text + '\x1b[%sm' % RESET
 return ( '\x1b[%sm' % ';' . join ( i1I1ii1II1iII ) ) + text
 if 13 - 13: Oo0Ooo . i11iIiiIii - iIii1I11I1II1 - OoOoOO00
def make_style ( opts = ( ) , ** kwargs ) :
 return lambda ii1I : colorize ( ii1I , opts , ** kwargs )
 if 76 - 76: O0 / o0oOOo0O0Ooo . I1IiiI * Ii1I - OOooOOo
 if 76 - 76: i11iIiiIii / iIii1I11I1II1 . I1ii11iIi11i % OOooOOo / OoooooooOO % oO0o
 if 75 - 75: iII111i
 if 97 - 97: i11iIiiIii
 if 32 - 32: Oo0Ooo * O0 % oO0o % Ii1I . IiII
 if 61 - 61: ooOoO0o
 if 79 - 79: Oo0Ooo + I1IiiI - iII111i
 if 83 - 83: ooOoO0o
 if 64 - 64: OoO0O00 % ooOoO0o % iII111i / OoOoOO00 - OoO0O00
 if 74 - 74: iII111i * O0
NOCOLOR_PALETTE = 'nocolor'
DARK_PALETTE = 'dark'
LIGHT_PALETTE = 'light'
if 89 - 89: oO0o + Oo0Ooo
PALETTES = {
 NOCOLOR_PALETTE : {
 'ERROR' : { } ,
 'NOTICE' : { } ,
 'SQL_FIELD' : { } ,
 'SQL_COLTYPE' : { } ,
 'SQL_KEYWORD' : { } ,
 'SQL_TABLE' : { } ,
 'HTTP_INFO' : { } ,
 'HTTP_SUCCESS' : { } ,
 'HTTP_REDIRECT' : { } ,
 'HTTP_NOT_MODIFIED' : { } ,
 'HTTP_BAD_REQUEST' : { } ,
 'HTTP_NOT_FOUND' : { } ,
 'HTTP_SERVER_ERROR' : { } ,
 } ,
 DARK_PALETTE : {
 'ERROR' : { 'fg' : 'red' , 'opts' : ( 'bold' , ) } ,
 'NOTICE' : { 'fg' : 'yellow' } ,
 'SQL_FIELD' : { 'fg' : 'green' , 'opts' : ( 'bold' , ) } ,
 'SQL_COLTYPE' : { 'fg' : 'green' } ,
 'SQL_KEYWORD' : { 'fg' : 'yellow' } ,
 'SQL_TABLE' : { 'opts' : ( 'bold' , ) } ,
 'HTTP_INFO' : { 'opts' : ( 'bold' , ) } ,
 'HTTP_SUCCESS' : { } ,
 'HTTP_REDIRECT' : { 'fg' : 'green' } ,
 'HTTP_NOT_MODIFIED' : { 'fg' : 'cyan' } ,
 'HTTP_BAD_REQUEST' : { 'fg' : 'red' , 'opts' : ( 'bold' , ) } ,
 'HTTP_NOT_FOUND' : { 'fg' : 'yellow' } ,
 'HTTP_SERVER_ERROR' : { 'fg' : 'magenta' , 'opts' : ( 'bold' , ) } ,
 } ,
 LIGHT_PALETTE : {
 'ERROR' : { 'fg' : 'red' , 'opts' : ( 'bold' , ) } ,
 'NOTICE' : { 'fg' : 'yellow' } ,
 'SQL_FIELD' : { 'fg' : 'green' , 'opts' : ( 'bold' , ) } ,
 'SQL_COLTYPE' : { 'fg' : 'green' } ,
 'SQL_KEYWORD' : { 'fg' : 'blue' } ,
 'SQL_TABLE' : { 'opts' : ( 'bold' , ) } ,
 'HTTP_INFO' : { 'opts' : ( 'bold' , ) } ,
 'HTTP_SUCCESS' : { } ,
 'HTTP_REDIRECT' : { 'fg' : 'green' , 'opts' : ( 'bold' , ) } ,
 'HTTP_NOT_MODIFIED' : { 'fg' : 'green' } ,
 'HTTP_BAD_REQUEST' : { 'fg' : 'red' , 'opts' : ( 'bold' , ) } ,
 'HTTP_NOT_FOUND' : { 'fg' : 'red' } ,
 'HTTP_SERVER_ERROR' : { 'fg' : 'magenta' , 'opts' : ( 'bold' , ) } ,
 }
 }
DEFAULT_PALETTE = DARK_PALETTE
if 3 - 3: i1IIi / I1IiiI % I11i * i11iIiiIii / O0 * I11i
def parse_color_setting ( config_string ) :
 if not config_string :
  if 49 - 49: oO0o % Ii1I + i1IIi . I1IiiI % I1ii11iIi11i
  if 48 - 48: I11i + I11i / II111iiii / iIii1I11I1II1
  if 20 - 20: o0oOOo0O0Ooo
  if 77 - 77: OoOoOO00 / I11i
  if 98 - 98: iIii1I11I1II1 / i1IIi / i11iIiiIii / o0oOOo0O0Ooo
  if 28 - 28: OOooOOo - IiII . IiII + OoOoOO00 - OoooooooOO + O0
  if 95 - 95: OoO0O00 % oO0o . O0
  if 15 - 15: ooOoO0o / Ii1I . Ii1I - i1IIi
  if 53 - 53: IiII + I1IiiI * oO0o
  if 61 - 61: i1IIi * OOooOOo / OoooooooOO . i11iIiiIii . OoOoOO00
  if 60 - 60: I11i / I11i
  if 46 - 46: Ii1I * OOooOOo - OoO0O00 * oO0o - I1Ii111
  if 83 - 83: OoooooooOO
  if 31 - 31: II111iiii - OOooOOo . I1Ii111 % OoOoOO00 - O0
  if 4 - 4: II111iiii / ooOoO0o . iII111i
  if 58 - 58: OOooOOo * i11iIiiIii / OoOoOO00 % I1Ii111 - I1ii11iIi11i / oO0o
  if 50 - 50: I1IiiI
  if 34 - 34: I1IiiI * II111iiii % iII111i * OoOoOO00 - I1IiiI
  if 33 - 33: o0oOOo0O0Ooo + OOooOOo * OoO0O00 - Oo0Ooo / oO0o % Ii1I
  if 21 - 21: OoO0O00 * iIii1I11I1II1 % oO0o * i1IIi
  if 16 - 16: O0 - I1Ii111 * iIii1I11I1II1 + iII111i
  if 50 - 50: II111iiii - ooOoO0o * I1ii11iIi11i / I1Ii111 + o0oOOo0O0Ooo
  if 88 - 88: Ii1I / I1Ii111 + iII111i - II111iiii / ooOoO0o - OoOoOO00
  if 15 - 15: I1ii11iIi11i + OoOoOO00 - OoooooooOO / OOooOOo
  if 58 - 58: i11iIiiIii % I11i
  if 71 - 71: OOooOOo + ooOoO0o % i11iIiiIii + I1ii11iIi11i - IiII
  if 88 - 88: OoOoOO00 - OoO0O00 % OOooOOo
  if 16 - 16: I1IiiI * oO0o % IiII
  if 86 - 86: I1IiiI + Ii1I % i11iIiiIii * oO0o . ooOoO0o * I11i
  return PALETTES [ DEFAULT_PALETTE ]
  if 44 - 44: oO0o
  if 88 - 88: I1Ii111 % Ii1I . II111iiii
 iI1ii1Ii = config_string . lower ( ) . split ( ';' )
 oooo000 = PALETTES [ NOCOLOR_PALETTE ] . copy ( )
 for iIIIi1 in iI1ii1Ii :
  if iIIIi1 in PALETTES :
   if 20 - 20: i1IIi + I1ii11iIi11i - ooOoO0o
   oooo000 . update ( PALETTES [ iIIIi1 ] )
  elif '=' in iIIIi1 :
   if 30 - 30: II111iiii - OOooOOo - i11iIiiIii % OoOoOO00 - II111iiii * Ii1I
   oO00O0O0O = { }
   if 31 - 31: I11i - II111iiii . I11i
   if 18 - 18: o0oOOo0O0Ooo
   if 98 - 98: iII111i * iII111i / iII111i + I11i
   if 34 - 34: ooOoO0o
   I1111I1iII11 , Oooo0O0oo00oO = iIIIi1 . split ( '=' )
   I1111I1iII11 = I1111I1iII11 . upper ( )
   if 14 - 14: OoOoOO00 / IiII . OoOoOO00 . I11i % OoO0O00 * I11i
   iII = Oooo0O0oo00oO . split ( ',' )
   iII . reverse ( )
   if 96 - 96: Oo0Ooo
   if 45 - 45: O0 * o0oOOo0O0Ooo % Oo0Ooo * OoooooooOO + iII111i . OoOoOO00
   if 67 - 67: i11iIiiIii - i1IIi % I1ii11iIi11i . O0
   o0oo = iII . pop ( ) . split ( '/' )
   o0oo . reverse ( )
   oooooOoo0ooo = o0oo . pop ( )
   if oooooOoo0ooo in color_names :
    oO00O0O0O [ 'fg' ] = oooooOoo0ooo
   if o0oo and o0oo [ - 1 ] in color_names :
    oO00O0O0O [ 'bg' ] = o0oo [ - 1 ]
    if 6 - 6: I11i - Ii1I + iIii1I11I1II1 - I1Ii111 - i11iIiiIii
    if 79 - 79: OoOoOO00 - O0 * OoO0O00 + OoOoOO00 % O0 * O0
   oOOo0 = tuple ( s for s in iII if s in opt_dict . keys ( ) )
   if oOOo0 :
    oO00O0O0O [ 'opts' ] = oOOo0
    if 54 - 54: O0 - IiII % OOooOOo
    if 77 - 77: OoOoOO00 / I1IiiI / OoO0O00 + OoO0O00 . OOooOOo
    if 38 - 38: I1Ii111
    if 7 - 7: O0 . iII111i % I1ii11iIi11i - I1IiiI - iIii1I11I1II1
   if I1111I1iII11 in PALETTES [ NOCOLOR_PALETTE ] and oO00O0O0O :
    oooo000 [ I1111I1iII11 ] = oO00O0O0O
    if 36 - 36: IiII % ooOoO0o % Oo0Ooo - I1ii11iIi11i
    if 22 - 22: iIii1I11I1II1 / Oo0Ooo * I1ii11iIi11i % iII111i
 if oooo000 == PALETTES [ NOCOLOR_PALETTE ] :
  return None
 return oooo000
# dd678faae9ac167bc83abf78e5cb2f3f0688d3a3
