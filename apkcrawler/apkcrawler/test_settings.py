# -*- coding: utf-8 -*-
# Scrapy settings for apkcrawler project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
BOT_NAME = 'apkcrawler'
SPIDER_MODULES = ['apkcrawler.spiders']
NEWSPIDER_MODULE = 'apkcrawler.spiders'
ITEM_PIPELINES =  {
    #'apkcrawler.scrapy_redis.pipelines.RedisPipeline': 10,
    #'apkcrawler.pipelines.APKFilesPipeline': 21,
    #'apkcrawler.pipelines.MongoDBPipeline':802,
    #'apkcrawler.pipelines.PostPipeline': 803,
}
LOG_LEVEL = 'DEBUG'#WARNING,INFO,DEBUG
MONGODB_URI = "mongodb://crawler:crawler@127.0.0.1:27017"
MONGODB_DB = "APK_Crawler"
MONGODB_COLLECTION = "AppStore"
IMAGES_STORE = '/home/ubuntu/Desktop/python_dev/mobile_app_crawler/apkcrawler/apkData'
FILES_STORE = '/var/appstore'
A4P_HOST_URL = 'http://192.168.0.12:80/API/GNAUpload'
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'apkcrawler (+http://www.yourdomain.com)'
HTTP_PROXIES = [
    {'ip_port': '192.168.0.10', 'user_pass': ''},
    #{'ip_port': '111.11.228.75:80', 'user_pass': ''},
    #{'ip_port': '120.198.243.22:80', 'user_pass': ''},
    #{'ip_port': '111.8.60.9:8123', 'user_pass': ''},
    #{'ip_port': '101.71.27.120:80', 'user_pass': ''},
    #{'ip_port': '122.96.59.104:80', 'user_pass': ''},
    #{'ip_port': '122.224.249.122:8088', 'user_pass': ''},
]

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS=32

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY=10
# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN=16
#CONCURRENT_REQUESTS_PER_IP=8

# Disable cookies (enabled by default)
COOKIES_ENABLED=False
RETRY_ENABLED = True
RETRY_TIMES = 3
RETRY_HTTP_CODES = [500, 502, 503, 504, 400, 408]
REFERER_ENABLED = True
# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED=False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
    #'apkcrawler.middlewares.ProxyMiddleware': 1,
    #'apkcrawler.middlewares.MyCustomSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    #'apkcrawler.middlewares.MyCustomDownloaderMiddleware': 543,
    'apkcrawler.middlewares.useragent.RotateUserAgentMiddleware': 9,
    #'apkcrawler.middlewares.middlewares.ProxyMiddleware': 10,
}

# Enable or disable extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'apkcrawler.pipelines.SomePipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
# NOTE: AutoThrottle will honour the standard settings for concurrency and delay
#AUTOTHROTTLE_ENABLED=True
# The initial download delay
#AUTOTHROTTLE_START_DELAY=5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY=60
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG=False
# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED=True
#HTTPCACHE_EXPIRATION_SECS=0
#HTTPCACHE_DIR='httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES=[]
#HTTPCACHE_STORAGE='scrapy.extensions.httpcache.FilesystemGNAUploadCacheStorage'

# Enables scheduling storing requests queue in redis.
#SCHEDULER = "apkcrawler.scrapy_redis.scheduler.Scheduler"
# Don't cleanup redis queues, allows to pause/resume crawls.
#SCHEDULER_PERSIST = True
# Schedule requests using a priority queue. (default)
#SCHEDULER_QUEUE_CLASS = 'apkcrawler.scrapy_redis.queue.SpiderPriorityQueue'
# Schedule requests using a queue (FIFO).
#SCHEDULER_QUEUE_CLASS = 'apkcrawler.scrapy_redis.queue.SpiderQueue'
# Schedule requests using a stack (LIFO).
#SCHEDULER_QUEUE_CLASS = 'apkcrawler.scrapy_redis.queue.SpiderStack'
# Max idle time to prevent the spider from being closed when distributed crawling.
# This only works if queue class is SpiderQueue or SpiderStack,
# and may also block the same time when your spider start at the first time (because the queue is empty).
#SCHEDULER_IDLE_BEFORE_CLOSE = 10
# Store scraped item in redis for post-processing.

# Specify the host and port to use when connecting to Redis (optional).
#REDIS_HOST = '127.0.0.1'
#REDIS_PORT = 6379
# Specify the full Redis URL for connecting (optional).
# If set, this takes precedence over the REDIS_HOST and REDIS_PORT settings.
#REDIS_URL = 'redis://:951@localhost:6379'#'redis://user:951@localhost:6379'
#REDIS_URL = 'redis://user:pass@hostname:9001'


DOWNLOAD_TIMEOUT=600