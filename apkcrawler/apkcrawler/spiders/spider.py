# -*- coding: utf-8 -*-
__authors__ = 'allen chen.20160.3.20,iverson,rison'
#123
import re,json
import scrapy
from scrapy.selector import Selector
from apkcrawler.items import ApkcrawlerItem
import urlparse
from scrapy.spiders import CrawlSpider
from bs4 import BeautifulSoup
import requests
import urllib2
from selenium import webdriver

class WanDouJia(scrapy.Spider):
    name = "wandoujia"
    allowed_domains = ["www.wandoujia.com"]
    start_urls = ['http://www.wandoujia.com/category/app']

    def __init__(self, project_id=''):
        super(WanDouJia, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        for clink in sel.css('html body.appcategory.PC div.container ul.clearfix.tag-box li.parent-cate a.cate-link ::attr(href)').extract():
            yield scrapy.Request(clink, self.parse_apklst)

    def parse_apklst(self, response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        for apklink in sel.css('html body.taginfo.PC.app div.container div.cards-wrap div.cols.clearfix div.col-left ul#j-tag-list.app-box.clearfix li.card div.app-desc h2.app-title-h2 a.name ::attr(href)').extract():
            yield scrapy.Request(apklink, self.parse_apkDetail,meta={'item':item})
        next_page = ''.join(sel.css('html body.taginfo.PC.app div.container div.cards-wrap div.cols.clearfix div.col-left div.pagination div.page-wp.roboto a.page-item.next-page ::attr(href)').extract())
        if next_page: yield scrapy.Request(next_page, self.parse_apklst)

    def parse_apkDetail(self, response):
        item = response.meta['item']
        sel = Selector(response)
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'wandoujia'
        item['package_name'] = (response.url.split('/')[-1])
        item['name'] = ''.join(sel.css('html body.detail.PC div.container div.detail-wrap div.detail-top.clearfix div.app-info p.app-name span.title ::text').extract())
        item['icon_url'] = ''.join(sel.css('html body.detail.PC div.container div.detail-wrap div.detail-top.clearfix div.app-icon img ::attr(src)').extract())
        item['apk_download'] = ''.join(sel.css('html body.detail.PC div.container div.detail-wrap div.detail-top.clearfix div.app-info div.download-wp a.install-btn ::attr(href)').extract())
        item['size'] = ''.join(sel.xpath('//dl[contains(@class, "infos-list")]/dd[1]/text()').extract()).strip()
        item['version'] = ''.join(sel.xpath('//dl[contains(@class, "infos-list")]/dd[5]/text()').extract()).strip()
        item['download_number'] = ''.join(sel.css('html body.detail.PC div.container div.detail-wrap div.detail-top.clearfix div.num-list span.item i ::text').extract()).strip()
        return item

class Baidu(scrapy.Spider):#CrawlSpider #scrapy.Spider
    name = "baidu"
    # COOKIES_ENABLED=True
    allowed_domains = ["shouji.baidu.com"]
    start_urls = ['http://shouji.baidu.com/game/','http://shouji.baidu.com/software']#

    def __init__(self, project_id=''):
        super(Baidu, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # (第一層)取分類的連結(/game/401_board_100_0101/)從第一個類別開始到最後-->進入第二層(分類頁)
        for category in sel.xpath('//ul[contains(@class,"cate-body")]/li/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category),callback= self.parse_apk_link,headers={'Referer':url})
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/game/9256604.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"app-box")]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link),callback= self.parse_apkDetail,headers={'Referer':url},meta={'item': item})
            # print '>>>>>>category>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//li[contains(@class,"next")]/a')[-1:]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'>' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'baidu'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name(沒有)


        # 名稱
        name = ''.join(sel.xpath('//h1[contains(@class,"app-name")]/span/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"app-pic")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//*[@id="doc"]/div[2]/div/div[1]/div/div[5]/span/@data_url').extract())
        if apk_download and apk_download != '0':
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"detail")]/span[1]/text()').extract()).split(u' ')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//span[contains(@class,"version")]/text()').extract()).split(u' ')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"detail")]/span[3]/text()').extract()).split(u' ')[-1]
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        return item



class QQ(scrapy.Spider):
    name = "qq"
    allowed_domains = ["android.app.qq.com"]
    start_urls = ['http://android.app.qq.com/category.htm?orgame=1&categoryId=-10',]

    def __init__(self, project_id=''):
        super(QQ, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        for category in sel.xpath('//li[contains(@id,"cate")]/a/@href').extract():
           c_l = response.url.split('?')[0]+category
           yield scrapy.Request(c_l, self.parse_apklst)

    def parse_apklst(self, response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        for apk_s in sel.xpath('//a[contains(@class,"name ofh")]'):
            apk_link = ''.join(apk_s.xpath('.//@href').extract())
            item['name'] = ''.join(apk_s.xpath('.//text()').extract())
            item['package_name'] = apk_link.split('=')[-1]
            yield scrapy.Request('http://android.app.qq.com/'+apk_link, self.parse_apkDetail,meta={'item':item})

    def parse_apkDetail(self, response):#
        sel = Selector(response)
        item = response.meta['item']
        item['version'] = ''.join(sel.xpath('/html/body/div[3]/div/div[3]/div[2]/text()').extract())
        item['creator'] = ''.join(sel.xpath('/html/body/div[3]/div/div[3]/div[6]/text()').extract())
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'qq'
        item['icon_url'] = ''.join(sel.css('html body div#J_DetDataContainer.com-container div.det-main-container div.det-ins-container.J_Mod div.det-icon img ::attr(src)').extract())
        item['download_number'] = ''.join(sel.css('html body div#J_DetDataContainer.com-container div.det-main-container div.det-ins-container.J_Mod div.det-ins-data div.det-insnum-line div.det-ins-num ::text').extract())
        item['size'] = ''.join(sel.css('html body div#J_DetDataContainer.com-container div.det-main-container div.det-ins-container.J_Mod div.det-ins-data div.det-insnum-line div.det-size ::text').extract())
        item['apk_download'] = ''.join(sel.css('html body div#J_DetDataContainer.com-container div.det-main-container div.det-ins-container.J_Mod div.det-ins-btn-box a.det-down-btn ::attr(data-apkurl)').extract())
        item['rate'] = ''.join(sel.css('html body div#J_DetDataContainer.com-container div.det-main-container div.det-ins-container.J_Mod div.det-ins-data div.det-star-box div.com-blue-star-num ::text').extract())
        return item


#手機版的
class Easou(scrapy.Spider):
    name = "easou"
    allowed_domains = ["app.easou.com"]
    start_urls = ['http://app.easou.com/gcatalog?esid=VxcK4vjFostgKH&wver=ta&uc=0103030000&com=w&pn=undefined']

    def __init__(self, project_id=''):
        super(Easou, self)
        self.project_id = project_id

    def parse(self, response):
        item = ApkcrawlerItem()
        type_url = 'http://app.easou.com/catalogResult?subModule=0624008?esid=VxcK4vjFostgKH&wver=ta&uc=0103040000&q='
        page_url = '&com=w&m=1&pn='
        rt_url = '&rt=2'

        jsonresponse = json.loads(response.body)['gameCatalogs']
        for json_data in jsonresponse:
            page_number = 1
            while (1):
                #print type_url + json_data['title'] + page_url + str(page_number) + rt_url

                test_response = requests.get(type_url + json_data['title'] + page_url + str(page_number) +rt_url)
                test_response_catalogResults =json.loads(test_response.text)['catalogResults']

                if test_response_catalogResults:
                    #print type_url + json_data['title'] + page_url + str(page_number) +rt_url
                    yield scrapy.Request(type_url + json_data['title'] + page_url + str(page_number) +rt_url, self.parseLink, meta={'item': item})
                    page_number = page_number + 1
                else:
                    break

    def parseLink(self, response):

        jsonresponse = json.loads(response.body)['catalogResults']
        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'easou'

        #print jsonresponse
        for json_data in jsonresponse:
            #item['creator'] = json_data['cnsite']  # 開發者
            item['creator'] = json_data['fields']['cnsite']  # 開發者
            item['version'] = json_data['fields']['versionName']  # 版本
            item['size'] = json_data['fields']['pkgSize']  # 大小
            item['icon_url'] = json_data['icon']
            item['apk_download'] = json_data['dlUrl']  # 下載
            item['download_number'] = json_data['fields']['dlCountNorm']
            item['name'] = json_data['title']  # 名稱
            item['package_name'] = json_data['title']  # 名稱
            item['rate'] = json_data['score']  # 評分

        return item


# 機鋒（403）
class Gfan(scrapy.Spider):
    name = "gfan"
    allowed_domains = ["apk.gfan.com"]
    start_urls = ['http://apk.gfan.com/appss_7_1_1.html','http://apk.gfan.com/gamess_8_1_1.html']#

    def __init__(self, project_id=''):
        super(Gfan, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # (第一層)取分類的連結(/apps_1_1_1.html)從第二個類別開始到最後;第一個"全部分類"有問題，所以從第二個開始抓-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"soft-cat div-border clearfix")]/a/@href')[1:].extract():#[12:13]僅7頁做測試
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link,headers={'Referer': url})
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        url = response.url
        # (第二層)取apk的連結(/Product/App1098741.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//ul[contains(@class,"lp-app-list clearfix")]/li/a/@href')[0:].extract():
            yield scrapy.Request('http://apk.gfan.com/' + apk_link, self.parse_apkDetail, meta={'item': item})
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//li[contains(@class,"next")]/a')[-1:]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"descr-right")]/h4/text()').extract())#名稱
        # print name
        # version = ''.join(sel.xpath('//div[contains(@class,"app-info")]/p[1]/text()').extract())#版本
        # print version.split(u'：')[-1]
        # creator = ''.join(sel.xpath('//div[contains(@class,"app-info")]/p[2]/text()').extract())#開發者
        # print creator.split(u'：')[-1]
        # date = ''.join(sel.xpath('//div[contains(@class,"app-info")]/p[3]/text()').extract())#日期
        # print date.split(u'：')[-1]
        # size = ''.join(sel.xpath('//div[contains(@class,"app-info")]/p[4]/text()').extract())#大小
        # print size.split(u'：')[-1]
        # support_firmware = ''.join(sel.xpath('//div[contains(@class,"app-info")]/p[6]/text()').extract())#支持固件
        # print support_firmware.split(u'：')[-1].split(u' ')[0]
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"app-view-bt")]/a/@href').extract())#下載
        # print apk_download
        # apk_score = ''.join(sel.xpath('//span[contains(@class,"app-marking png")]/text()').extract())#評分
        # print apk_score
        # apk_scorefrequency = ''.join(sel.xpath('//span[contains(@class,"marking-tips")]/text()').extract()).strip()#評分次數
        # print apk_scorefrequency.split(u'次')[0]

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'gfan'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name(沒有)


        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"descr-right")]/h4/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"t-c p-t20")]/span/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"app-view-bt")]/a/@href').extract())
        if apk_download and apk_download != '0':
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"app-info")]/p[4]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"app-info")]/p[1]/text()').extract()).split(u'：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//span[contains(@class,"marking-tips")]/text()').extract()).split(u'次')[0]
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        return item
#appchina (部分302)
class appchina(scrapy.Spider):
    name = "appchina"
    allowed_domains = ["www.appchina.com"]
    start_urls = ['http://www.appchina.com/category/30.html','http://www.appchina.com/category/40.html']#
    #start_urls = ['http://www.appchina.com/category/30.html']

    def __init__(self, project_id=''):
        super(appchina, self)
        self.project_id = project_id

    def parse(self, response):
        sel = BeautifulSoup(response.body)
        item = ApkcrawlerItem()
        for clink in sel.select('.view-detail'):
            apklink = "http://www.appchina.com" +clink['href']
            yield scrapy.Request(apklink, callback=self.parseLink, meta={'item': item})
        next_page = 'http://www.appchina.com' + sel.select('.next')[0]['href']
        if next_page: yield scrapy.Request(next_page, self.parse)

    def parseLink(self, response):
        sel = BeautifulSoup(response.body)
        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'appchina'

        #print sel.select('.app-name')[0].text # 名稱
        item['package_name'] = sel.select('.app-name')[0].text  # 名稱
        item['name'] = sel.select('.app-name')[0].text # 名稱
        #print sel.select('.download_app')[0]['href']
        item['apk_download'] = sel.select('.download_app')[0]['href'] #下載
        item['download_number'] = '0'
        #print sel.select('.Content_Icon')[0]['src']
        item['icon_url'] = sel.select('.Content_Icon')[0]['src']

        for info in  sel.select('.app-other-info-intro .art-content'):
            if '大小' in info.text.encode('utf-8'):
                print info.text.encode('utf-8').replace('大小：','').replace(' ','')
                item['size'] = info.text.encode('utf-8').replace('大小：','').replace(' ','')# 大小
            elif '更新' in info.text.encode('utf-8'):
                print '更新'+info.text.encode('utf-8').replace('更新：', '').replace(' ', '')
                #item['date'] = info.text.encode('utf-8').replace('更新：', '').replace(' ', '')# 日期
            elif '版本' in info.text.encode('utf-8'):
                print info.text.encode('utf-8').replace('版本：', '').replace(' ', '')
                item['version'] = info.text.encode('utf-8').replace('版本：', '').replace(' ', '')# 版本
            elif '开发者' in info.text.encode('utf-8'):
                print info.text.encode('utf-8').replace('开发者：', '').replace(' ', '')
                item['creator'] = info.text.encode('utf-8').replace('开发者：', '').replace(' ', '')# 開發者

        return item

# tvhuan
class tvhuan(scrapy.Spider):
    name = "tvhuan"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["app.tvhuan.com"]
    start_urls = ['http://app.tvhuan.com/app-1442.html']

    def __init__(self, project_id=''):
        super(tvhuan, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/app-1442.html)從第二個類別開始到最後;第一個"全部分類"有問題，所以從第二個開始抓-->進入第二層(分類頁)
        for category in sel.xpath('//ul[contains(@class,"tabhd fc")]/li/a/@href')[1:-1].extract():
            yield scrapy.Request(urlparse.urljoin(url, category.strip('./')), callback=self.parse_category,headers={'Referer': url})
            # print '>>>>>>111>>>>>>>>>' + category.strip('./')

    def parse_category(self, response):
        sel = Selector(response)
        url = response.url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/game/9256604.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//ul[contains(@class,"listhot")]/li/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})
            # print '>>>>>>111>>>>>>>>>' + apk_link
        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//a[contains(@class,"pre")][2]/@href').extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page.strip('./')), callback=self.parse_category,headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"selfadapt")]/h1/text()').extract())#名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"detail fc")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"dbtn")]/a[2]/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"right")]/ul/li[1]/text()').extract())  # 大小
        # print size.split(u'：')[-1]
        # version = ''.join(sel.xpath('//div[contains(@class,"right")]/ul/li[3]/text()').extract())#版本
        # print version.split(u'：')[-1]
        # download_number = ''.join(sel.xpath('//div[contains(@class,"dbtn")]/span/i/text()').extract())  # 評分次數
        # print download_number.strip(u'次').strip(u'+')


        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'tvhuan'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name(沒有)


        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"selfadapt")]/h1/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"detail fc")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"dbtn")]/a[2]/@href').extract())
        if apk_download and apk_download != '0':
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"right")]/ul/li[1]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"right")]/ul/li[3]/text()').extract()).split(u'：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"dbtn")]/span/i/text()').extract()).strip(u'次').strip(u'+')
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        return item

#d9soft()
class d9soft(scrapy.Spider):
    name ='d9soft'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.d9soft.com"]  # 不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://www.d9soft.com/android/game/list-933-597.html','http://www.d9soft.com/android/app/list-932-2.html']#

    def __init__(self, project_id=''):
        super(d9soft, self)
        self.project_id = project_id

    def parse(self,response):
        sel = Selector(response)
        url = response.url
        # item = ApkcrawlerItem()

        for apk_link in sel.xpath('//div[contains(@class,"list-right")]/p/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail, headers={'Referer': url})  #,meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"page")]/a/@href')[-1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse, headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page
    def parse_apkDetail(self,response):
        item = ApkcrawlerItem()
        # item = response.meta['item']
        sel = Selector(response)
        res = BeautifulSoup(response.body)

        # name = res.select('h1')[0].text
        # print name
        # for item01 in res.select('.de-head-l'):
        #     icon_url= item01.select('img')[0]['src']
        # print icon_url
        # size = ''.join(sel.xpath('//ul[contains(@class, "de-game-info clearfix")]/li[4]/text()').extract())
        # print size
        # version = ''.join(sel.xpath('//ul[contains(@class, "de-game-info clearfix")]/li[2]/text()').extract())
        # print version
        # apk_download= res.select('.de-pc-btn')[0]['href']
        # print apk_download
        # download_number= res.select('.de-score-int')[0].text
        # print download_number


        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'd9soft'
        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        #無package_name
        #apk名字
        if res.select('h1')[0].text:
            if res.select('h1')[0].text != 0:
                item['name'] = res.select('h1')[0].text
        #icon
        for item01 in res.select('.de-head-l'):
            if item01.select('img')[0]['src']:
                if item01.select('img')[0]['src'] != 0:
                    item['icon_url'] = item01.select('img')[0]['src']
        # 下載
        if res.select('.de-pc-btn')[0]['href']:
            if res.select('.de-pc-btn')[0]['href'] != 0:
                item['apk_download'] = res.select('.de-pc-btn')[0]['href']
        #大小
        if ''.join(sel.xpath('//ul[contains(@class, "de-game-info clearfix")]/li[4]/text()').extract()):
            if ''.join(sel.xpath('//ul[contains(@class, "de-game-info clearfix")]/li[4]/text()').extract()) != 0:
                item['size'] = ''.join(sel.xpath('//ul[contains(@class, "de-game-info clearfix")]/li[4]/text()').extract())

        #版本
        if ''.join(sel.xpath('//ul[contains(@class, "de-game-info clearfix")]/li[2]/text()').extract()):
            if ''.join(sel.xpath('//ul[contains(@class, "de-game-info clearfix")]/li[2]/text()').extract()) != 0:
                item['version'] = ''.join(sel.xpath('//ul[contains(@class, "de-game-info clearfix")]/li[2]/text()').extract())

        # 評分次數
        if res.select('.de-score-int')[0].text:
            if res.select('.de-score-int')[0].text != 0:
                item['download_number'] = res.select('.de-score-int')[0].text
        return item


# nerease163
class nerease163(scrapy.Spider):
    name = "nerease163"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["m.163.com"]#不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://m.163.com/android/game/allgame/index.html','http://m.163.com/android/category/allapp/index.html']#

    def __init__(self, project_id=''):
        super(nerease163, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/android/game/ca96b6hqnd/index.html)從第一個類別開始到倒數第二個;最後一個"全部分類"頁數有問題-->進入第二層(分類頁)
        for category in sel.xpath('//li[contains(@class,"fl-l p-h10 p-b10")]/a/@href')[0:-1].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})#
            # print '>>>>>>111>>>>>>>>>' + category

    def parse_category(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>aaaaaa'
        item = ApkcrawlerItem()
        # (第一層)取apk的連結(/game/9256604.html)-->進入第二層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"apps-info-img")]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"yui3-appsview-page")]/a/@href')[-1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category,headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"sect-main-s-inner p-t15")]/h1/span/text()').extract())#名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"t-c p-t20")]/span/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"t-c p-t20")]/p[1]/a/@href').extract()).replace('%3A',':').replace('%2F','/').split(u'url=')[-1]   # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"t-c p-t20")]/p[5]/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"t-c p-t20")]/p[6]/text()').extract()).split(u'：')[-1]   # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"fl-l m-r15")]/span[2]/text()').extract()).strip(u'()')  # 評分次數
        # print download_number

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'nerease163'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name(沒有)


        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"sect-main-s-inner p-t15")]/h1/span/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"t-c p-t20")]/span/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"t-c p-t20")]/p[1]/a/@href').extract()).replace('%3A',':').replace('%2F','/').split(u'url=')[-1]
        if apk_download and apk_download != '0':
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[5]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"t-c p-t20")]/p[6]/text()').extract()).split(u'：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"fl-l m-r15")]/span[2]/text()').extract()).strip(u'()')
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        return item





# oppo(apk-.js已解)(503) Service Unavailable建議download_delay；(302)(404)為apk連結有問題
class oppo(scrapy.Spider):
    name = "oppo"
    # download_delay = 10
    allowed_domains = ["store.oppomobile.com"]
    start_urls = ['http://store.oppomobile.com/product/category/12_7_1.html',
                  'http://store.oppomobile.com/product/category/12_8_1.html']

    def __init__(self, project_id=''):
        super(oppo, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print 'url>>>>>>>>'+url
        item = ApkcrawlerItem()
        for apk_link in sel.xpath('//div[contains(@class,"li_tubiao")]/a/@href')[0:].extract():  # 取apk的連結/Product/App1098741.html
            # print 'apk_link>>>>>>>>'+apk_link
            yield scrapy.Request(urlparse.urljoin(url, apk_link), self.parse_apkDetail, meta={'item': item})  #

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//li[contains(@class,"next")]/a/@href').extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"soft_info_middle")]/h3/text()').extract())#名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"soft_info_left")]/img/@dataimg').extract())  # icon
        # print icon_url
        # apk_download_id = ''.join(sel.xpath('//a[contains(@class,"detail_down")]/@onclick').extract()).split(u'(')[-1].strip(u')')  # 下載
        # apk_download_from = ''.join(sel.xpath('//div[contains(@class,"login_content")]/ul/li[1]/a/@href').extract()).split(u'from%3D')[-1]
        # apk_download = 'http://store.oppomobile.com/product/download.html?id='+apk_download_id+'&from='+apk_download_from
        # print apk_download
        # size = ''.join(sel.xpath('//ul[contains(@class,"soft_info_more")]/li[2]/text()').extract())  # 大小
        # print size.split(u'：')[-1]
        # version = ''.join(sel.xpath('//ul[contains(@class,"soft_info_more")]/li[3]/text()').extract())#版本
        # print version.split(u'：')[-1]
        # download_number = ''.join(sel.xpath('//div[contains(@class,"soft_info_nums")]/text()').extract())  # 下載次數
        # print download_number.strip().split(u'+')[0]
        # creator = ''.join(sel.xpath('//div[contains(@class,"origin-wrap")]/a/text()').extract())#開發者
        # print creator.split(u':')[-1]

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'oppo'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name(沒有)

        # 名稱
        if ''.join(sel.xpath('//div[contains(@class,"soft_info_middle")]/h3/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"soft_info_middle")]/h3/text()').extract()) != '0':
                item['name'] = ''.join(sel.xpath('//div[contains(@class,"soft_info_middle")]/h3/text()').extract())  # 名稱

        # icon
        if ''.join(sel.xpath('//div[contains(@class,"soft_info_left")]/img/@dataimg').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"soft_info_left")]/img/@dataimg').extract()) != '0':
                item['icon_url'] = ''.join( sel.xpath('//div[contains(@class,"soft_info_left")]/img/@dataimg').extract())  # icon

        # 下載
        if ''.join(sel.xpath('//a[contains(@class,"detail_down")]/@onclick').extract()):
            if ''.join(sel.xpath('//a[contains(@class,"detail_down")]/@onclick').extract()) != '0':
                apk_download_id = ''.join(sel.xpath('//a[contains(@class,"detail_down")]/@onclick').extract()).split(u'(')[-1].strip(u')')
                apk_download_from = ''.join(sel.xpath('//div[contains(@class,"login_content")]/ul/li[1]/a/@href').extract()).split(u'from%3D')[-1]
                item['apk_download'] = 'http://store.oppomobile.com/product/download.html?id=' + apk_download_id + '&from=' + apk_download_from  # 下載

        # 大小
        if ''.join(sel.xpath('//ul[contains(@class,"soft_info_more")]/li[2]/text()').extract()):
            if ''.join(sel.xpath('//ul[contains(@class,"soft_info_more")]/li[2]/text()').extract()) != '0':
                item['size'] = ''.join(sel.xpath('//ul[contains(@class,"soft_info_more")]/li[2]/text()').extract()).split(u'：')[-1]  # 大小

        # 版本
        if ''.join(sel.xpath('//ul[contains(@class,"soft_info_more")]/li[3]/text()').extract()):
            if ''.join(sel.xpath('//ul[contains(@class,"soft_info_more")]/li[3]/text()').extract()) != '0':
                item['version'] = ''.join(sel.xpath('//ul[contains(@class,"soft_info_more")]/li[3]/text()').extract()).split(u'：')[-1]  # 版本

        # 評分次數
        if ''.join(sel.xpath('//div[contains(@class,"soft_info_nums")]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"soft_info_nums")]/text()').extract()) != '0':
                item['download_number'] = ''.join(sel.xpath('//div[contains(@class,"soft_info_nums")]/text()').extract()).strip().split(u'+')[0]  # 評分次數
        return item

# anruan(DEBUG: Redirecting (303))
class anruan(scrapy.Spider):
    name = "anruan"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["anruan.com"]#不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://soft.anruan.com/','http://game.anruan.com/']#

    def __init__(self, project_id=''):
        super(anruan, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(http://soft.anruan.com/type_xitong_1.html)-->進入第二層(分類頁)
        for category in sel.xpath('/html/body/div[4]/div[2]/div[2]/div[1]/div/div[2]/ul/li/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})
            print '>>>>>>111>>>>>>>>>' + category

    def parse_category(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>aaaaaa'
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/game/9256604.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('/html/body/div[4]/div[2]/div[1]/div/div/ul/li/div/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#
            print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        # for next_page in sel.xpath('//div[contains(@class,"pager")]/a/@href')[-1:].extract():
        #     yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category,headers={'Referer': url})
        #     print '>>>>>>next_page>>>>>>>>>' +next_page


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"ps pc")]/h1/text()').extract()) # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"app_info_img")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//li[contains(@class,"app_down")]/a/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[6]/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[4]/text()').extract()).strip(u'+') # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[7]/text()').extract())  # 評分次數
        # print download_number


        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'anruan'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name(沒有)


        # 名稱
        if ''.join(sel.xpath('//div[contains(@class,"ps pc")]/h1/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"ps pc")]/h1/text()').extract()) != '0':
                item['name'] = ''.join(sel.xpath('//div[contains(@class,"ps pc")]/h1/text()').extract())  # 名稱

        # icon
        if ''.join(sel.xpath('//div[contains(@class,"app_info_img")]/img/@src').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"app_info_img")]/img/@src').extract()) != '0':
                item['icon_url'] = ''.join(sel.xpath('//div[contains(@class,"app_info_img")]/img/@src').extract())  # icon

        # 下載
        if ''.join(sel.xpath('//li[contains(@class,"app_down")]/a/@href').extract()):
            if ''.join(sel.xpath('//li[contains(@class,"app_down")]/a/@href').extract()) != '0':
                item['apk_download'] = ''.join(sel.xpath('//li[contains(@class,"app_down")]/a/@href').extract())  # 下載

        # 大小
        if ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[6]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[6]/text()').extract()) != '0':
                item['size'] = ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[6]/text()').extract())  # 大小

        # 版本
        if ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[4]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[4]/text()').extract()) != '0':
                item['version'] = ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[4]/text()').extract()).strip(u'+') # 版本

        # 評分次數
        if ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[7]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[7]/text()').extract()) != '0':
                item['download_number'] = ''.join(sel.xpath('//div[contains(@class,"mdqc")]/ul/li[7]/text()').extract())  # 評分次數
        return item

#ruan8 (302)
class ruan8(scrapy.Spider):
    name ='ruan8'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ['ruan8.com']
    start_urls = ['http://www.ruan8.com/android/','http://mgame.ruan8.com/']

    def __init__(self, project_id=''):
        super(ruan8, self)
        self.project_id = project_id

    def parse(self,response):
        sel = BeautifulSoup(response.body)
        url = response.url

        for apk_link in  sel.select('.mdzq a'):
            if apk_link.text.encode('utf-8') != '热门下载' and apk_link.text.encode('utf-8') != '最新上传':
                # print 'http://www.ruan8.com'+ apk_link['href']
                yield scrapy.Request(urlparse.urljoin(url, apk_link['href']), callback=self.parse_apkpage,headers={'Referer': url})
                break

    def parse_apkpage(self,response):
        sel = BeautifulSoup(response.body)
        url = response.url

        for apk_link in sel.select('.listname a'):
            # print 'http://www.ruan8.com' + apk_link['href']
            yield scrapy.Request(urlparse.urljoin(url, apk_link['href']), callback=self.parse_apkdetail,headers={'Referer': url})
            # break
        for next_pages in sel.select('.mdpage a'):
            if '下一页' in next_pages.text.encode('utf-8'):
                yield scrapy.Request(urlparse.urljoin(url, next_pages['href']), callback=self.parse_apkpage,headers={'Referer': url})

    def parse_apkdetail(self,response):
        item = ApkcrawlerItem()
        sel = BeautifulSoup(response.body)

        if '安卓版下载' in sel.select('.dbb a')[0].text.encode('utf-8'):
            item['project_id'] = self.project_id if self.project_id else 1
            item['apk_store'] = 'ruan8'
            item['package_name'] = 'zero'
            item['name'] = 'zero'
            item['icon_url'] = 'zero'
            item['apk_download'] = 'zero'
            item['size'] = 'zero'
            item['version'] = 'zero'
            item['download_number'] = 'zero'

            if sel.select('h1')[0].text != '0' and sel.select('h1')[0].text:
                item['name'] = sel.select('h1')[0].text
                # print sel.select('h1')[0].text
            if sel.select('#thumb img')[0]['src'] != '0' and sel.select('#thumb img')[0]['src']:
                item['icon_url'] = sel.select('#thumb img')[0]['src']
                # print sel.select('#thumb img')[0]['src']
            if sel.select('.dbb a')[0]['href'] != '0' and sel.select('.dbb a')[0]['href']:
                apk_download = 'http://www.ruan8.com'+ sel.select('.dbb a')[0]['href']
                apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
                item['apk_download'] = apk_download
                # print 'http://www.ruan8.com'+ sel.select('.dbb a')[0]['href']
            for infos in sel.select('.mdccs li'):
                if '大小' in infos.text.encode('utf-8'):
                    if infos.text.encode('utf-8').split('：')[-1] != '0' and infos.text.encode('utf-8').split('：')[-1]:
                        item['size'] = infos.text.encode('utf-8').split('：')[-1]
                        # print infos.text.encode('utf-8').split('：')[-1]
                elif '版本' in infos.text.encode('utf-8'):
                    if infos.text.encode('utf-8').split('：')[-1] != '0' and infos.text.encode('utf-8').split(':')[-1]:
                        item['version'] = infos.text.encode('utf-8').split('：')[-1]
                        # print infos.text.encode('utf-8').split('：')[-1]
                elif '下载' in infos.text.encode('utf-8'):
                    if infos.text.encode('utf-8').split('：')[-1] != '0' and infos.text.encode('utf-8').split('：')[-1]:
                        item['download_number'] = infos.text.encode('utf-8').split('：')[-1]
                        # print infos.text.encode('utf-8').split('：')[-1]
            return item


# youxibaba(mongoDB-OK,APKFile不行；跑太快會(503) Service Unavailable建議download_delay；(302)(404)為apk連結有問題)
class youxibaba(scrapy.Spider):
    name = "youxibaba"
    # download_delay = 3
    # COOKIES_ENABLED = True
    allowed_domains = ["app.youxibaba.cn"]#不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://app.youxibaba.cn/soft/applist/cid/9']#遊戲＆軟件類別可在同一網址擷取

    def __init__(self, project_id=''):
        super(youxibaba, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/soft/applist/cid/10)-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"sideBar-categories-content")]/ul/li/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})#
            # print '>>>>>>111>>>>>>>>>' + category

    def parse_category(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>aaaaaa'
        item = ApkcrawlerItem()
        # (第一層)取apk的連結(/app/info/appid/334632)-->進入第二層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"content-categoryCtn-content clearfix")]/div/div[1]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//a[contains(@class,"page-item next-page")]/@href')[-1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category,headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"content-categoryCtn-title clearfix")]/h1/text()').extract())#名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"content-detailCtn-icon")]/p/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"content-detailCtn-icon")]/a/@href').extract())   # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"sideBar")]/ul/li[3]/div/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"sideBar")]/ul/li[2]/div/text()').extract()).split(u'：')[-1]   # 版本
        # print version
        # download_number = 'zero'  # 評分次數
        # print download_number
        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'youxibaba'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name
        if ''.join(sel.xpath('//div[contains(@class,"content-categoryCtn-title clearfix")]/h1/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"content-categoryCtn-title clearfix")]/h1/text()').extract()) != '0':
                item['package_name'] = ''.join(sel.xpath('//div[contains(@class,"content-categoryCtn-title clearfix")]/h1/text()').extract())

        # 名稱
        if ''.join(sel.xpath('//div[contains(@class,"content-categoryCtn-title clearfix")]/h1/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"content-categoryCtn-title clearfix")]/h1/text()').extract()) != '0':
                item['name'] = ''.join(sel.xpath('//div[contains(@class,"content-categoryCtn-title clearfix")]/h1/text()').extract())  # 名稱

        # icon
        if ''.join(sel.xpath('//div[contains(@class,"content-detailCtn-icon")]/p/img/@src').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"content-detailCtn-icon")]/p/img/@src').extract()) != '0':
                item['icon_url'] = ''.join(sel.xpath('//div[contains(@class,"content-detailCtn-icon")]/p/img/@src').extract())  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"content-detailCtn-icon")]/a/@href').extract())
        if apk_download and apk_download != '0':
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載
        # 大小
        if ''.join(sel.xpath('//div[contains(@class,"sideBar")]/ul/li[3]/div/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"sideBar")]/ul/li[3]/div/text()').extract()) != '0':
                item['size'] = ''.join(sel.xpath('//div[contains(@class,"sideBar")]/ul/li[3]/div/text()').extract())  # 大小

        # 版本
        if ''.join(sel.xpath('//div[contains(@class,"sideBar")]/ul/li[2]/div/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"sideBar")]/ul/li[2]/div/text()').extract()) != '0':
                item['version'] = ''.join(sel.xpath('//div[contains(@class,"sideBar")]/ul/li[2]/div/text()').extract()) # 版本

        # 評分次數
        item['download_number'] = 'zero' # 評分次數
        return item


# app7to(302)
class app7to(scrapy.Spider):
    name = "app7to"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["app.7to.cn"]#不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://app.7to.cn/game.html','http://app.7to.cn/app.html']#

    def __init__(self, project_id=''):
        super(app7to, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(game_34.html)第一個是全部(從第二個開始抓)-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"sc_box6")]/ul/li/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category.strip(' ')), callback=self.parse_category,headers={'Referer': url})
            # print '>>>>>>111>>>>>>>>>' + category

    def parse_category(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>aaaaaa'
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/419783.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"sc_box5")]/ul/li/div/span/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"sc_box5")]/ul/li/div/a/@href')[-1:].extract():
            if next_page == 'javascript:void(0);':  # 到末頁時依舊有”下一頁“，故判斷遇javascript:void(0);停止for
                break
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category,headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"box7_fr")]/dl/dt/h1/text()').extract())#名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"box7_fl")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"xz")]/a[1]/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"wz")]/span[3]/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"wz")]/span[1]/text()').extract()).split(u'：')[-1]  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"wz")]/span[2]/text()').extract()).split(u'：')[-1]  # 評分次數
        # print download_number


        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'app7to'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name
        if ''.join(sel.xpath('//div[contains(@class,"box7_fr")]/dl/dt/h1/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"box7_fr")]/dl/dt/h1/text()').extract()) != '0':
                item['package_name'] = ''.join(sel.xpath('//div[contains(@class,"box7_fr")]/dl/dt/h1/text()').extract())

        # 名稱
        if ''.join(sel.xpath('//div[contains(@class,"box7_fr")]/dl/dt/h1/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"box7_fr")]/dl/dt/h1/text()').extract()) != '0':
                item['name'] = ''.join(sel.xpath('//div[contains(@class,"box7_fr")]/dl/dt/h1/text()').extract())  # 名稱

        # icon
        if ''.join(sel.xpath('//div[contains(@class,"box7_fl")]/img/@src').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"box7_fl")]/img/@src').extract()) != '0':
                item['icon_url'] = ''.join(sel.xpath('//div[contains(@class,"box7_fl")]/img/@src').extract())  # icon

        # 下載
        if ''.join(sel.xpath('//div[contains(@class,"xz")]/a[1]/@href').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"xz")]/a[1]/@href').extract()) != '0':
                item['apk_download'] = ''.join(sel.xpath('//div[contains(@class,"xz")]/a[1]/@href').extract())  # 下載

        # 大小
        if ''.join(sel.xpath('//div[contains(@class,"wz")]/span[3]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"wz")]/span[3]/text()').extract()) != '0':
                item['size'] = ''.join(sel.xpath('//div[contains(@class,"wz")]/span[3]/text()').extract()).split(u'：')[-1]  # 大小

        # 版本
        if ''.join(sel.xpath('//div[contains(@class,"wz")]/span[1]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"wz")]/span[1]/text()').extract()) != '0':
                item['version'] = ''.join(sel.xpath('//div[contains(@class,"wz")]/span[1]/text()').extract()).split(u'：')[-1] # 版本

        # 評分次數
        if ''.join(sel.xpath('//div[contains(@class,"wz")]/span[2]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"wz")]/span[2]/text()').extract()) != '0':
                item['download_number'] = ''.join(sel.xpath('//div[contains(@class,"wz")]/span[2]/text()').extract()).split(u'：')[-1]  # 評分次數
        return item

# appginoee
class appginoee(scrapy.Spider):
    name = 'appginoee'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.appgionee.com"]  # 不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://www.appgionee.com/apps-1-0-1-1784/', 'http://www.appgionee.com/games-2-0-1-1/']  #

    def __init__(self, project_id=''):
        super(appginoee, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # item = ApkcrawlerItem()
        for apk_link in sel.xpath('//div[contains(@class,"app_txt z")]/p/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,
                                 headers={'Referer': url})  # ,meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"app_page")]/a/@href')[-1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse, headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page

    def parse_apkDetail(self, response):
        item = ApkcrawlerItem()
        # item = response.meta['item']
        sel = Selector(response)
        res = BeautifulSoup(response.body)
        res.encoding = 'utf-8'
        # #33~46為print出
        # name = res.select('h1')[0].text.split('(')[0].encode('utf-8')
        # print name #名稱
        # for icon in res.select('.content1_top_pic.z'):
        #     print icon.select('img')[0]['src'] #icon_url
        # domain_download = 'http://www.appgionee.com'
        # apk_download = domain_download + sel.css('html body.iscontent div.wrap div.wp div.content div.content_left.z div.content1 div.content1_bottom a::attr(href)').extract()[0]
        # print apk_download  # 下載
        # information = res.select('.ctxt')[0]
        # size = information.select('span')[3].text.encode('utf-8').split('：')[1]
        # print size #大小
        # version = res.select('h1')[0].text.split('(')[1].split(')')[0].encode('utf-8')
        # print version #版本
        # download_number = information.select('span')[2].text.encode('utf-8').split('：')[1]
        # print download_number #評分次數

        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'appginoee'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        # package name(無)
        item['package_name'] = 'zero'
        # 名稱
        name = res.select('h1')[0].text.split('(')[0].encode('utf-8')
        if name and name != 0:
            item['name'] = name
        # icon_url
        for icon in res.select('.content1_top_pic.z'):
            icon_url = icon.select('img')[0]['src']
            if icon_url and icon_url != 0:
                item['icon_url'] = icon_url
        # 下載(瀏覽器判定為惡意網站，抓取apk名稱前面附加下載的網站)
        domain_download = 'http://www.appgionee.com'
        apk_download = domain_download + sel.css(
            'html body.iscontent div.wrap div.wp div.content div.content_left.z div.content1 div.content1_bottom a::attr(href)').extract()[
            0]
        if apk_download and apk_download != 0:
            item['apk_download'] = apk_download
        information = res.select('.ctxt')[0]
        # 大小
        size = information.select('span')[3].text.encode('utf-8').split('：')[1]
        if size and size != 0:
            item['size'] = size
        # 版本
        version = res.select('h1')[0].text.split('(')[1].split(')')[0].encode('utf-8')
        if version and version != 0:
            item['version'] = version
        # 評分次數
        download_number = information.select('span')[2].text.encode('utf-8').split('：')[1]
        if download_number and download_number != 0:
            item['download_number'] = download_number
        return item

# sz1001(部分載點會載百度可能不會自動下載)APK download 403
class sz1001(scrapy.Spider):
    name ='sz1001'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.sz1001.net"]
    start_urls = ['http://www.sz1001.net/sort/341_1.htm']

    def __init__(self, project_id=''):
        super(sz1001, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(第一個是全部,所以由[1:]開始)
        for category in sel.xpath('//p[contains(@class,"two_nav")]/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})#
            # print '>>>>>>111>>>>>>>>>' + category
    def parse_category(self, response):
        sel = Selector(response)
        url = response.url
        domain_icon = 'http://www.sz1001.net'
        # # (第一層)取apk的連結-->進入第二層 截取apk內容資料
        for apk_s in sel.xpath('//*[@id="list_content"]/div/div[1]/a')[0:]:
            item = ApkcrawlerItem()
            item['icon_url'] = 'zero'
            apk_link = ''.join(apk_s.xpath('.//@href').extract())
            icon_url = domain_icon + ''.join(apk_s.xpath('.//img/@src').extract())
            if icon_url and icon_url != 0:
                item['icon_url'] = icon_url
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
            # print '>>>>>>icon_link>>>>>>>>>' + icon_url
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"tsp_nav")]/a/@href')[-2:-1].extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category,headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' + next_page
    def parse_apkDetail(self, response):
        sel = Selector(response)
        item = response.meta['item']
        res = BeautifulSoup(response.body)
        res.encoding = 'gbk'
    # #52~64行為print出
    #     category = ''.join(sel.xpath('//*[@id="nav"]/span/a[4]/text()').extract())
    #     print category #分類
    #     name = ''.join(sel.xpath('//*[@id="softtitle"]/text()').extract())
    #     print name  #名稱
    #     apk_download = ''.join(sel.xpath('//*[@id="download"]/div[1]/ul/li/a/@href')[-1].extract())
    #     print apk_download #下載
    #     size = ''.join(sel.xpath('//*[@id="c_soft_down"]/div[3]/ul/li[1]/text()').extract()).split(u': ')[-1]
    #     print size #大小
    #     version = ''.join(sel.xpath('//*[@id="softtitle"]/text()').extract())
    #     print version #版本(無版本但大部分在名稱內，用名稱代替)
    #     for rate in res.select('.c_soft_info.m-soft-introd'):
    #         download_number = str(rate.select('li')[1].select('img')[0]).split('.png')[0].split('lv')[-1]
    #         print download_number # 評分次數
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'sz1001'
        # item['category'] ='zero'
        item['package_name'] = 'zero' # package name(無)
        item['name'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero' # 版本(無版本,但版本在name裡,用name代替)
        item['download_number'] = 'zero' #評分
        item['creator'] = 'zero' #無開發者
        # 分類
        # category = ''.join(sel.xpath('//*[@id="nav"]/span/a[4]/text()').extract())
        # if category and category != 0:
        #     item['category'] = category
        # 名稱
        name = ''.join(sel.xpath('//*[@id="softtitle"]/text()').extract())
        if name and name != 0:
            item['name'] = name
        # 下載
        apk_download = ''.join(sel.xpath('//*[@id="download"]/div[1]/ul/li/a/@href')[-1].extract())
        if apk_download and apk_download != 0:
            item['apk_download'] = apk_download
        #大小
        size = ''.join(sel.xpath('//*[@id="c_soft_down"]/div[3]/ul/li[1]/text()').extract()).split(u': ')[-1]
        if size and size != 0:
            item['size'] = size
        # 版本(無版本,但版本在name裡,用name代替)
        version = ''.join(sel.xpath('//*[@id="softtitle"]/text()').extract())
        if version and version != 0:
            item['version'] = version
        # 評分次數
        for rate in res.select('.c_soft_info.m-soft-introd'):
            download_number=str(rate.select('li')[1].select('img')[0]).split('.png')[0].split('lv')[-1]
            if download_number and download_number != 0:
                item['download_number'] = download_number
        return item



# skycn
class skycn(scrapy.Spider):
    name = 'skycn'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.skycn.com"]
    start_urls = ['http://www.skycn.com/soft/17_rank_1.html']

    def __init__(self, project_id=''):
        super(skycn, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>aaaaaa'
        # #item = ApkcrawlerItem()
        # (第一層)取apk的連結(/game/9256604.html)-->進入第二層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"span4 g-left")]/ul/li/a[2]/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,
                                 headers={'Referer': url})  # , meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' +'http://www.skycn.com'+apk_link
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"tsp_nav")]/a/@href')[-2:-1].extract():
            # 下一頁與總頁數-1相等時終止
            a178 = ''.join(sel.xpath('//div[contains(@class,"tsp_nav")]/a/text()')[-1:].extract())
            page_end_b = str(int(''.join(sel.xpath('//div[contains(@class,"tsp_nav")]/b/text()').extract())) - 1)
            # print 'a178>>>>'+a178
            # print 'int>>>>'+ str(int(''.join(sel.xpath('//div[contains(@class,"tsp_nav")]/b/text()').extract()))-1)
            if page_end_b == a178:
                break
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse, headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' + next_page

    def parse_apkDetail(self, response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        res = BeautifulSoup(response.body)
        res.encoding = 'gbk'
        domain = 'http://skycnxz2.wy119.com'  # 用於下載的domain,apk檔名開頭無/
        domain01 = 'http://skycnxz2.wy119.com/'  # 用於下載的domain01,apk檔名開頭有/
        domain_icon = 'http://www.skycn.com'
        # 43~60行為print出
        # name = ''.join(sel.xpath('//div[contains(@class,"span4 g-game-introd")]/h1/text()').extract())
        # print name #名稱
        # icon_url = domain_icon + ''.join(sel.xpath('//div[contains(@class,"span3 g-game-img")]/img/@src').extract())
        # print icon_url #icon_url
        # for item02 in res.select('.ul_Address'):
        #     if '/' in str(item02.text):
        #         apk_download = domain + str(item02.text).split('Address:"')[-1].split('"')[0]
        #         print apk_download #下載
        #     else:
        #         apk_download = domain01 + str(item02.text).split('Address:"')[-1].split('"')[0]
        #         print apk_download #下載
        # size = ''.join(sel.xpath('//div[contains(@class,"span4 g-game-introd")]/ul/li[1]/span/text()').extract())
        # print size #大小
        # version = ''.join(sel.xpath('//div[contains(@class,"span4 g-game-introd")]/h1/text()').extract())
        # print version #版本用名稱代替
        # for rate in res.select('.g-introd'):
        #     download_number = rate.select('li')[2].select('img')[0]['src'].split('rank')[-1].split('.')[0]
        #     print download_number # 評分次數
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'skycn'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        # package name(無)
        item['package_name'] = 'zero'
        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"span4 g-game-introd")]/h1/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon_url
        icon_url = ''.join(sel.xpath('//div[contains(@class,"span3 g-game-img")]/img/@src').extract())
        if icon_url and icon_url != 0:
            item['icon_url'] = domain_icon + icon_url
        # 下載(瀏覽器判定為惡意網站，抓取apk名稱前面附加下載的網站)
        for item02 in res.select('.ul_Address'):
            if str(item02.text):
                if str(item02.text) != 0:
                    if '/' in str(item02.text):
                        item['apk_download'] = domain + str(item02.text).split('Address:"')[-1].split('"')[0]
                    else:
                        item['apk_download'] = domain01 + str(item02.text).split('Address:"')[-1].split('"')[0]
        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"span4 g-game-introd")]/ul/li[1]/span/text()').extract())
        if size and size != 0:
            item['size'] = size
        # 版本(無版本,但版本在name裡,用name代替)
        version = ''.join(sel.xpath('//div[contains(@class,"span4 g-game-introd")]/h1/text()').extract())
        if version and version != 0:
            item['version'] = version
        # 評分次數
        for rate in res.select('.g-introd'):
            download_number = rate.select('li')[2]
            if download_number and download_number != 0:
                item['download_number'] = rate.select('li')[2].select('img')[0]['src'].split('rank')[-1].split('.')[0]
        return item


# itools(DEBUG: Redirecting (302)，download_number在上層 但py順序不定 寫到DB值會不正確)
class itools(scrapy.Spider):
    name = "itools"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["android.itools.cn"]#不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://android.itools.cn/app','http://android.itools.cn/game']#

    def __init__(self, project_id=''):
        super(itools, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        item = ApkcrawlerItem()

        # a=-1
        # (第二層)取apk的連結(/419783.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"ios_app_on")]/p/a/@href')[0:].extract():
            # a=a+1
            # download_number = ''.join(sel.xpath('//div[contains(@class,"ios_box wrap_app_list")]/ul/li/div/p[3]/span/text()')[a].extract())
            # print download_number
            # if ''.join(sel.xpath('//div[contains(@class,"ios_box wrap_app_list")]/ul/li/div/p[3]/span/text()')[a].extract()):
            #     if ''.join(sel.xpath('//div[contains(@class,"ios_box wrap_app_list")]/ul/li/div/p[3]/span/text()')[a].extract()) != '0':
            #         item['download_number'] = ''.join(sel.xpath('//div[contains(@class,"ios_box wrap_app_list")]/ul/li/div/p[3]/span/text()')[a].extract())
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url},meta={'item': item} )#
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"pages clearfix")]/a/@href')[-1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse,headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page


    def parse_apkDetail(self, response):
        sel = Selector(response)
        url = response.url

        # package_name = url.split(u'/')[-1]
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dt/text()').extract()) # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"fl w140")]/p/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"fl w140")]/a/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dd[2]/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dd[3]/text()').extract())  # 版本
        # print version
        # download_number = 'zero'  # 評分次數
        # print download_number



        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'itools'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'

        # package_name
        package_name = url.split(u'/')[-1]
        if package_name and package_name != '0':
            item['package_name'] = package_name

        # 名稱
        if ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dt/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dt/text()').extract()) != '0':
                item['name'] = ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dt/text()').extract())  # 名稱

        # icon
        if ''.join(sel.xpath('//div[contains(@class,"fl w140")]/p/img/@src').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"fl w140")]/p/img/@src').extract()) != '0':
                item['icon_url'] = ''.join(sel.xpath('//div[contains(@class,"fl w140")]/p/img/@src').extract())  # icon

        # 下載
        if ''.join(sel.xpath('//div[contains(@class,"fl w140")]/a/@href').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"fl w140")]/a/@href').extract()) != '0':
                item['apk_download'] = ''.join(sel.xpath('//div[contains(@class,"fl w140")]/a/@href').extract())  # 下載

        # 大小
        if ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dd[2]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dd[2]/text()').extract()) != '0':
                item['size'] = ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dd[2]/text()').extract())  # 大小

        # 版本
        if ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dd[3]/text()').extract()):
            if ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dd[3]/text()').extract()) != '0':
                item['version'] = ''.join(sel.xpath('//div[contains(@class,"details_app")]/dl/dd[3]/text()').extract()).split(u'：')[-1] # 版本

        # 評分次數(download_number在上層 但py順序不定 寫到DB值會不正確 故先設'zero')
        # if ''.join(sel.xpath('//div[contains(@class,"wz")]/span[2]/text()').extract()):
        #     if ''.join(sel.xpath('//div[contains(@class,"wz")]/span[2]/text()').extract()) != '0':
        #         item['download_number'] = ''.join(sel.xpath('//div[contains(@class,"wz")]/span[2]/text()').extract()).split(u'：')[-1]  # 評分次數
        return item



# pp25
class pp25(CrawlSpider):
    name = "pp25"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.25pp.com"]
    start_urls = ['http://www.25pp.com/android/','http://www.25pp.com/android/game/']#

    def __init__(self, project_id=''):
        super(pp25, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/android/soft/fenlei/5029/)從第一個類別開始到最後;-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"cate-list soft block-tab mt30")]/div[2]/ul/li/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category, headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_category(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>url>>>>>>>>>' + url
        # (第二層)取細部類別的連結(/android/soft/fenlei/5029_716/)全部類別有問題;從第二個類別開始到最後-->進入第三層(apk內容)
        for category2 in sel.xpath('/html/body/div[5]/div[2]/div[1]/ul/li/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category2), callback=self.parse_apk_link, headers={'Referer': url})#
            # print '>>>>>>>category2>>>>'+category2




    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>parse_apk_link>>>>>url>>>>>'+url
        item = ApkcrawlerItem()
        # (第三層)取apk的連結(/android/detail_27137/)-->進入第四層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"cate-list-main mt10")]/ul/li/a/@href')[0:1].extract():
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第三層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//a[contains(@class,"page-next")]/@href').extract():
            # print '>>>>>>next_page>>>>>>>>>' +next_page
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_apk_link,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"app-info")]/h1/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"detail-header clearfix")]/div/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"detail-side")]/div/a/@href').extract()).replace('http://android-apps.25pp.com/','http://ucdl.25pp.com/').split(u'?yingid')[0]  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"app-detail-info")]/p[1]/span[2]/strong/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"app-detail-info")]/p[2]/span[1]/strong/text()').extract())  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"app-install clearfix")]/div[2]/div/text()').extract()).split(u'下载')[0]  # 評分次數
        # print download_number
        # creator = 'zero'  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'pp25'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"app-info")]/h1/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"detail-header clearfix")]/div/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"detail-side")]/div/a/@href').extract()).replace('http://android-apps.25pp.com/','http://ucdl.25pp.com/').split(u'?yingid')[0]  # 下載
        if apk_download and apk_download != '0':
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"app-detail-info")]/p[1]/span[2]/strong/text()').extract())
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"app-detail-info")]/p[2]/span[1]/strong/text()').extract())
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"app-install clearfix")]/div[2]/div/text()').extract()).split(u'下载')[0]  # 評分次數
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者(沒有)

        return item


#dfone(302)
class dfone(scrapy.Spider):
    # download_delay = 10
    # COOKIES_ENABLED = True
    name ='dfone'
    allowed_domains = ["d-fone.com"]
    start_urls = ['http://www.d-fone.com/applist.php?action=downloadCount&sort=Soft&search_type=&mobile_type=Android&catname=']

    def __init__(self, project_id=''):
        super(dfone, self)
        self.project_id = project_id

    def parse(self,response):
        sel = BeautifulSoup(response.body)
        url = response.url
        item = ApkcrawlerItem()

        for apk_link in sel.select('.app_list_left a'):
            if apk_link.text.encode('utf-8') != '立即下载' and apk_link.text:
                yield scrapy.Request('http://www.d-fone.com/'+ apk_link['href'], callback=self.parse_detail,headers={'Referer': url}, meta={'item': item})

        for page_link in sel.select('.padpage a'):
            if '下一页' in page_link.text.encode('utf-8'):

                 yield scrapy.Request('http://www.d-fone.com/applist.php' + page_link['href'], callback=self.parse,headers={'Referer': url})

    def parse_detail(self, response):
        sel = BeautifulSoup(response.body)
        item = response.meta['item']
        url = response.url
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'dfone'
        item['package_name'] = 'zero'
        # item['category'] = 'zero'
        item['size'] = 'zero'
        item['creator'] = 'zero'
        item['download_number'] = 'zero'
        item['version'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        if sel.select('.dxt_ns')[0].text and sel.select('.dxt_ns')[0].text != 0:
            item['name'] = sel.select('.dxt_ns')[0].text

        if sel.select('.pad_topss img')[0]['src'] and  sel.select('.pad_topss img')[0]['src']!= 0:
            item['icon_url'] = sel.select('.pad_topss img')[0]['src']

        if sel.select('.detail_down')[0]['href'] and sel.select('.detail_down')[0]['href'] != 0:
            item['apk_download'] = sel.select('.detail_down')[0]['href']

        for a in sel.select('.det_link'):
            for info in str(a).split('<br/>'):
                # if '类别' in info.split('：')[0]:
                #     item['category'] = str(info.split('：')[-1]).replace('</td>','')
                if '作者' in info.split('：')[0]:
                    item['creator'] = str(info.split('：')[-1]).replace('</td>', '')
                elif '大小' in info.split('：')[0]:
                    item['size'] = str(info.split('：')[-1]).replace('</td>', '')
                elif '总下载次数' in info.split('：')[0]:
                    item['download_number'] = str(info.split('：')[-1]).replace('</td>', '')

        return item

#onefivefive(302)
class onefivefive(scrapy.Spider):
    name = 'onefivefive'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["android.155.cn"]
    start_urls = ['http://android.155.cn/game/']

    def __init__(self, project_id=''):
        super(onefivefive, self)
        self.project_id = project_id

    def parse(self, response):
        sel = BeautifulSoup(response.body)
        url = response.url


        for apk_type_page_link in sel.select('.sof_l_nav a'):
            if '全部' not in apk_type_page_link.text.encode('utf-8'):
                # item['category'] = apk_type_page_link.text.encode('utf-8')#類別
                yield scrapy.Request('http://android.155.cn' + apk_type_page_link['href'], callback=self.parse_category,headers={'Referer': url})


    def parse_category(self, response):
        sel = BeautifulSoup(response.body)
        url = response.url
        item = ApkcrawlerItem()
        for apk_link in sel.select('.yx_list_img a'):
            for apk_type in sel.select('.sof_l_nav a'):
                if url in 'http://android.155.cn' + apk_type['href']:
                    item['category'] = apk_type.text.encode('utf-8')#類別
            yield scrapy.Request('http://android.155.cn' + apk_link['href'], callback=self.parse_detail,headers={'Referer': url}, meta={'item': item})

            break
        # if sel.select('.page_next')[0].href:
        #     yield scrapy.Request('http://android.155.cn' + sel.select('.page_next')[0]['href'], callback=self.parse_category,headers={'Referer': url})

    def parse_detail(self, response):
        sel = BeautifulSoup(response.body)
        item = response.meta['item']
        url = response.url
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'onefivefive'
        item['package_name'] = 'zero'
        #item['category'] = 'zero'
        item['size'] = 'zero'
        item['creator'] = 'zero'
        item['download_number'] = 'zero'
        item['version'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'

        item['name'] = sel.select('h1')[0].text
        item['icon_url'] = sel.select('.left_icon_img img')[0]['data-original']
        item['apk_download'] = sel.select('#client_list a')[0]['href']

        for info in sel.select('.xinxi_p'):
            if '大小' in info.text.encode('utf-8'):
                item['size'] = info.select('span')[0].text
            elif '下载' in info.text.encode('utf-8'):
                item['download_number'] = info.select('span')[0].text
            elif '版本' in info.text.encode('utf-8'):
                item['version'] = info.select('span')[0].text

        for info2 in sel.select('.xinxi_p2'):
            if '开发者' in info2.text.encode('utf-8'):
                item['creator'] = info2.select('span')[0].text
        return item

# import urllib2
# gionee
class gionee(scrapy.Spider):
    name = "gionee"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["game.gionee.com"]
    start_urls = ['http://game.gionee.com/Front/Category/index/?cku=1239574343_null&action=visit&object=category&intersrc=']#

    def __init__(self, project_id=''):
        super(gionee, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(角色扮演)從第一個類別開始到倒數第二個，最後一個為全部(不用);-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"mod_box category_mod")]/ul/li/a/@href')[1:].extract():
            if 'game.gionee.com' in category:
                category = urllib2.urlopen(category, None, 8).geturl()

            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link, headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>parse_apk_link>>>>>url>>>>>'+url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(九仙渡劫)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"mod_box game_box")]/div[2]/ul/li/a/@href')[0:].extract():
            if 'game.gionee.com' in apk_link:
                apk_link = urllib2.urlopen(apk_link, None, 8).geturl()
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//a[contains(@class,"page-star")]'):
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            if u'下一页' in next_page_text:
                # print '>>>>>>next_page>>>>>>>>>' +next_page_text
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"game_intro")]/h4/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"game_intro")]/a/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//p[contains(@class,"btn_area")]/a[2]/@href').extract())  # 下載
        # # if 'game.gionee.com' in apk_link:
        # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
        # print apk_download
        # size = ''.join(sel.xpath('//span[contains(@class,"size")]/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//span[contains(@class,"versions")]/text()').extract()).split(u'：')[-1]  # 版本
        # print version
        # download_number = 'zero'
        # print download_number
        # creator = ''.join(sel.xpath('//div[contains(@class,"mod_box gameIntro_box")]/div[2]/ul/li[1]/text()').extract())  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'gionee'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"game_intro")]/h4/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"game_intro")]/a/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//p[contains(@class,"btn_area")]/a[2]/@href').extract())
        if apk_download and apk_download != '0':
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//span[contains(@class,"size")]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//span[contains(@class,"versions")]/text()').extract()).split(u'：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數(沒有)

        # 開發者
        creator = ''.join(sel.xpath('//div[contains(@class,"mod_box gameIntro_box")]/div[2]/ul/li[1]/text()').extract())
        if creator and creator != '0':
            item['creator'] = creator  # 版本

        return item


# xz7 (鎖IP ; 名稱 跟 版本 寫在一起 無法拆 ; 多載點 取广东电信 domain + xxx.apk ; icon 在上層 ; mondoDB重複，rides重複，但兩個內容不同步)
class xz7(scrapy.Spider):
    name = "xz7"
    download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.xz7.com"]
    start_urls = ['http://www.xz7.com/zhuanti/312_1.htm','http://www.xz7.com/soft/s_530_1.html']#

    def __init__(self, project_id=''):
        super(xz7, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/zhuanti/312_1.htm)從第一個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"list_aside")]/div[1]/ul/li/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link, headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>parse_apk_link>>>>>url>>>>>'+url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/dir/202970.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@id,"list_content")]/div/div[1]')[0:]:
            apk_link_href = ''.join(apk_link.xpath('.//a/@href').extract())
            apk_link_img = 'http://www.xz7.com/'+''.join(apk_link.xpath('.//a/img/@src').extract())
            icon_url = apk_link_img
            if icon_url and icon_url != '0':
                item['icon_url'] = icon_url
            # print '>>>>>>apk_link_href>>>>>>>>>' + apk_link_href
            # print '>>>>>>apk_link_img>>>>>>>>>' + apk_link_img
            yield scrapy.Request(urlparse.urljoin(url,apk_link_href), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#


        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        # for next_page in sel.xpath('//a[contains(@class,"tsp_next")]/@href').extract():
        #     print '>>>>>>next_page>>>>>>>>>' + next_page
        #     yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_apk_link,headers={'Referer': url})

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        # for next_page in sel.xpath('//div[contains(@class,"tsp_nav")]/a')[-2:-1]:
        #     next_page_text = ''.join(next_page.xpath('.//i/text()').extract())
        #     print '>>>>>>next_page>>>>>>>>>' + next_page_text
        #     if u'下一页' in next_page_text:
        #         next_page_href = ''.join(next_page.xpath('.//@href').extract())
        #         print '>>>>>>next_page>>>>>>>>>' + next_page_href
        #         yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # 名稱 跟 版本 寫在一起 無法拆
        # name = ''.join(sel.xpath('//div[contains(@id,"c_info_main")]/div[1]/h1/text()').extract())  # 名稱
        # print name
        # icon_url = '在上層'  # icon
        # print icon_url
        # # 多載點 取广东电信 domain + xxx.apk
        # apk_download = 'http://abc.xz7.com/'+''.join(sel.xpath('//ul[contains(@class,"ul_Address")]/script/text()').extract()).split(u'Address:"')[-1].split(u'",')[0]  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"c_soft_down_left")]/ul[1]/li[1]/strong[1]/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # # 名稱 跟 版本 寫在一起 無法拆
        # version = ''.join(sel.xpath('//div[contains(@id,"c_info_main")]/div[1]/h1/text()').extract())  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"c_soft_down_left")]/ul[2]/li[2]/span/text()').extract()).split(u':')[-1]  # 評分次數
        # print download_number
        # creator = ''.join(sel.xpath('//div[contains(@class,"c_soft_down_left")]/ul[1]/li[6]/strong[1]/text()').extract()).split(u'：')[-1]  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'xz7'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱 # 名稱 跟 版本 寫在一起 無法拆
        name = ''.join(sel.xpath('//div[contains(@id,"c_info_main")]/div[1]/h1/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon # 在上層
        icon_url = ''.join(sel.xpath('//div[contains(@class,"game_intro")]/a/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載 # 多載點 取广东电信 domain + xxx.apk
        apk_download = 'http://abc.xz7.com/'+''.join(sel.xpath('//ul[contains(@class,"ul_Address")]/script/text()').extract()).split(u'Address:"')[-1].split(u'",')[0]
        if apk_download and apk_download != '0':
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"c_soft_down_left")]/ul[1]/li[1]/strong[1]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本 # 名稱 跟 版本 寫在一起 無法拆
        version = ''.join(sel.xpath('//div[contains(@id,"c_info_main")]/div[1]/h1/text()').extract())
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"c_soft_down_left")]/ul[2]/li[2]/span/text()').extract()).split(u':')[-1]
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 版本

        # 開發者
        creator = ''.join(sel.xpath('//div[contains(@class,"c_soft_down_left")]/ul[1]/li[6]/strong[1]/text()').extract()).split(u'：')[-1]
        if creator and creator != ' ':
            item['creator'] = creator  # 開發者

        return item


# veryhd
class veryhd(scrapy.Spider):
    name = "veryhd"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["apk.veryhd.net"]
    start_urls = ['http://apk.veryhd.net:8080/soft_list_0_1_3.html','http://apk.veryhd.net:8080/game.html']#

    def __init__(self, project_id=''):
        super(veryhd, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(soft_list_48_1_3.html)第一個為所有類別，從第二個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"boxG-mid")]/ul/li/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link, headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/dir/202970.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"recSinW btmLine1")]/div//@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"pageNumber")]/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # name = ''.join(sel.xpath('//div[contains(@class,"boxTitleA")]/strong/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"appImg")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"downBtnMOLO")]/a/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"appPar")]/table/tr[3]/td[2]/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"appPar")]/table/tr[3]/td[3]/text()').extract())  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"appPar")]/table/tr[4]/td[1]/span/text()').extract())  # 評分次數
        # print download_number
        # creator = ''.join(sel.xpath('//div[contains(@class,"appPar")]/table/tr[4]/td[2]/text()').extract())  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'veryhd'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"boxTitleA")]/strong/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"appImg")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"downBtnMOLO")]/a/@href').extract())
        if apk_download and apk_download != '0':
            # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"appPar")]/table/tr[3]/td[2]/text()').extract())
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"appPar")]/table/tr[3]/td[3]/text()').extract())
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"appPar")]/table/tr[4]/td[1]/span/text()').extract())
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 版本

        # 開發者
        creator = ''.join(sel.xpath('//div[contains(@class,"appPar")]/table/tr[4]/td[2]/text()').extract())
        if creator and creator != ' ':
            item['creator'] = creator  # 開發者

        return item



# carapk (4類[導航、影音、系統、其他]，因只有'其他'有下一頁，故以其當test ; 跑很慢)
class carapk(scrapy.Spider):
    name = "carapk"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.carapk.com"]
    start_urls = ['http://www.carapk.com/app/others','http://www.carapk.com/app/map','http://www.carapk.com/app/player','http://www.carapk.com/app/systool']#

    def __init__(self, project_id=''):
        super(carapk, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第一層)取apk的連結(app_show_107748.html)-->進入第二層 截取apk內容資料
        for apk_link in sel.xpath('//span[contains(@class,"list02_title")]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})  #
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        # for next_page in sel.xpath('//a[contains(@class,"tsp_next")]/@href').extract():
        #     print '>>>>>>next_page>>>>>>>>>' + next_page
        #     yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse,headers={'Referer': url})

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"pagebox")]/a')[-1:]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"info-top_r")]/div[1]/span/text()').extract()).split(u'(')[0]  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"info-top_l")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"icog_butn03 pb15 white")]/a/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"info-params")]/table/tr[1]/td[1]/span/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"info-params")]/table/tr[1]/td[2]/span/text()').extract())  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"info-params")]/table/tr[2]/td[1]/span/text()').extract())  # 評分次數
        # print download_number
        # creator = 'zero'  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'carapk'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"info-top_r")]/div[1]/span/text()').extract()).split(u'(')[0]
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"info-top_l")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"icog_butn03 pb15 white")]/a/@href').extract())
        if apk_download and apk_download != '0':
            # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"info-params")]/table/tr[1]/td[1]/span/text()').extract())
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"info-params")]/table/tr[1]/td[2]/span/text()').extract())
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"info-params")]/table/tr[2]/td[1]/span/text()').extract())
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者(沒有)

        return item

# mogustore(分類換址位置不變，所以class不通用，故以軟件網址爬完軟件＆遊戲的所有分類)
class mogustore(scrapy.Spider):
    name = "mogustore"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.mogustore.com"]
    start_urls = ['http://www.mogustore.com/app.html']# 軟件與遊戲分類同一區拆開無法共用class，故以軟件網址爬完軟件＆遊戲的所有分類即可

    def __init__(self, project_id=''):
        super(mogustore, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(app_110.html)從第一個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"w238 main_left")]/div/div/div/ul/li/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link, headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(app_show_107748.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"pro_box app_list app_list_b")]/ul/li/div/h2/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url,apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})#
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        # for next_page in sel.xpath('//a[contains(@class,"tsp_next")]/@href').extract():
        #     print '>>>>>>next_page>>>>>>>>>' + next_page
        #     yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_apk_link,headers={'Referer': url})

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"page")]/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = ''.join(sel.xpath('//div[contains(@id,"app_down")]/ul/li/a/@href').extract()).split(u'download/')[-1].split(u'.apk')[0]
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"sub_title")]/h1/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@id,"app_title")]/div[1]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@id,"app_down")]/ul/li/a/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@id,"app_down")]/ul/li[1]/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"sub_title")]/ul/li[1]/text()').extract())  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"sub_title")]/ul/li[6]/text()').extract())  # 評分次數
        # print download_number
        # creator = ''.join(sel.xpath('//div[contains(@class,"sub_title")]/ul/li[3]/@title').extract())  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'mogustore'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(在下載裡面)
        package_name = ''.join(sel.xpath('//div[contains(@id,"app_down")]/ul/li/a/@href').extract()).split(u'download/')[-1].split(u'.apk')[0]
        if package_name and package_name != '0':
            item['package_name'] = package_name  # package_name

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"sub_title")]/h1/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@id,"app_title")]/div[1]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@id,"app_down")]/ul/li/a/@href').extract())
        if apk_download and apk_download != '0':
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@id,"app_down")]/ul/li[1]/text()').extract())
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"sub_title")]/ul/li[1]/text()').extract())
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"sub_title")]/ul/li[6]/text()').extract())
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者
        creator = ''.join(sel.xpath('//div[contains(@class,"sub_title")]/ul/li[3]/@title').extract())
        if creator and creator != ' ':
            item['creator'] = creator  # 開發者

        return item



#gamedog 已修改完成(只給軟件，網遊＆單機未上；大小與版本相同，且未抓到正確tag(span))
class gamedog(scrapy.Spider):
    name ='gamedog'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["android.gamedog.cn"]  # 不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://android.gamedog.cn/soft/','http://android.gamedog.cn/game/','http://android.gamedog.cn/online/','http://android.gamedog.cn/list_theme_0_0_0_0_0_132.html']#

    def __init__(self, project_id=''):
        super(gamedog, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結第1個為全部，從第2個類開始[1:]
        for category in sel.xpath('//div[contains(@class,"s_con")]/span/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})  #
            # print '>>>>>>111>>>>>>>>>' + category
    def parse_category(self,response):
        sel = Selector(response)
        url = response.url
        # item = ApkcrawlerItem()
        for apk_link in sel.xpath('//*[@id="contant"]/div[3]/div[2]/div/ul/li/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail, headers={'Referer': url})  #,meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//*[@id="contant"]/div[3]/div[2]/dl/dd/div/a/@href')[-1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category, headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page
    def parse_apkDetail(self,response):
        item = ApkcrawlerItem()
        # item = response.meta['item']
        sel = Selector(response)
        res = BeautifulSoup(response.body)
        res.encoding = 'utf-8'
        #40~53為print出
        # category = ''.join(sel.xpath('//div[contains(@class,"info_m")]/ul/li[2]/span/text()').extract())
        # print category #分類
        # name = ''.join(sel.xpath('//div[contains(@class,"game_info")]/dl[1]/dt/h1/span/text()').extract())
        # print name #名稱
        # icon = ''.join(sel.xpath('//div[contains(@class,"info_l")]/span[1]/img/@src').extract())
        # print icon #icon_url
        # apk_download = ''.join(sel.xpath('//*[@id="downs_box"]/span[1]/a/@href').extract())
        # print apk_download  # 下載
        # size = ''.join(sel.xpath('//div[contains(@class,"info_m")]/ul/li[3]/span/text()').extract())
        # print size #大小
        # version = res.select('.info_m')[0].select('li')[5].text.split(':')[-1]
        # print version #版本
        # creator = ''.join(sel.xpath('//li[contains(@class,"wd332")]/a/text()').extract())
        # print creator #開發者
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'gamedog'
        # item['category'] = 'zero'
        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero' #無評分次數
        item['package_name'] = 'zero' #無package name
        item['creator'] = 'zero'
        #分類
        # category = ''.join(sel.xpath('//div[contains(@class,"info_m")]/ul/li[2]/span/text()').extract())
        # if category and category != 0:
        #     item['category'] = category
        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"game_info")]/dl[1]/dt/h1/span/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon_url
        icon_url = ''.join(sel.xpath('//div[contains(@class,"info_l")]/span[1]/img/@src').extract())
        if icon_url and icon_url != 0:
            item['icon_url'] = icon_url
        # 下載
        apk_download =''.join(sel.xpath('//*[@id="downs_box"]/span[1]/a/@href').extract())
        if apk_download and apk_download != 0:
            item['apk_download'] = apk_download
        # 大小
        size = res.select('.info_m')[0].select('li')[2].select('span')[0].text
        if size and size != 0:
            item['size'] = size
        # 版本
        version = res.select('.info_m')[0].select('li')[5].select('span')[0].text
        if version and version != 0:
            item['version'] = version
        #開發者
        creator = ''.join(sel.xpath('//li[contains(@class,"wd332")]/a/text()').extract())
        if creator and creator != 0:
            item['creator']=creator
        return item

#sogou 可以跑()
class sogou(scrapy.Spider):
    name ='sogou'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["app.sogou.com"]
    start_urls = ['http://app.sogou.com/soft','http://app.sogou.com/game'] #

    def __init__(self, project_id=''):
        super(sogou, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層軟體)取分類的連結(第一個是全部,所以由[1:]開始)
        if 'soft' in url:
            for category in sel.xpath('//div[contains(@class,"list cf software")]/p/a/@href')[1:].extract():
                yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})#
                # print '>>>>>>111>>>>>>>>>' + category
        # (第一層遊戲)取分類的連結(第一個是全部,所以由[1:]開始)
        if 'game' in url:
            for category_02 in sel.xpath('//div[contains(@class,"list cf game")]/p/a/@href')[1:].extract():
                yield scrapy.Request(urlparse.urljoin(url, category_02), callback=self.parse_category,headers={'Referer': url})#
                # print '>>>>>>111>>>>>>>>>' + category_02

    def parse_category(self, response):
        sel = Selector(response)
        url = response.url
        for apk_link in sel.xpath('//ul[contains(@class,"cf")]/li/dl/dd/h3/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url})  # ,meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        #無分頁

    def parse_apkDetail(self, response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        res = BeautifulSoup(response.body)
        ## 42~57為print出
        # category = ''.join(sel.xpath('//div[contains(@class,"sub_nav cf")]/a[3]/text()').extract())
        # print category #分類
        # name = ''.join(sel.xpath('//div[contains(@class,"sub_nav cf")]/em/text()').extract())
        # print name #名稱
        # icon_url= res.select('.pic')[0].select('img')[0]['data-original']
        # print icon_url #icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"down_pc cf")]/a/@href').extract())
        # print apk_download #下載url
        # size = ''.join(sel.xpath('//ul[contains(@class, "dd cf")]/li[3]/text()').extract()).split(u'：')[-1]
        # print size #大小
        # version = ''.join(sel.xpath('//ul[contains(@class, "dd cf")]/li[4]/text()').extract()).split(u'：')[-1]
        # print version #版本
        # download_number = ''.join(sel.xpath('//ul[contains(@class, "dd cf")]/li[1]/text()').extract()).split(u'：')[-1]
        # print download_number #評分次數
        # creator = res.select('.content')[0].select('.tab3')[1]['title']
        # print creator #開發者
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'sogou'
        # item['category'] = 'zero'
        item['package_name'] = 'zero'  # 無package_name
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'  # 評分
        item['creator'] = 'zero'  # 無開發者
        # 分類
        # category = ''.join(sel.xpath('//div[contains(@class,"sub_nav cf")]/a[3]/text()').extract())
        # if category and category != 0:
        #     item['category'] = category
        # apk名字
        name = ''.join(sel.xpath('//div[contains(@class,"sub_nav cf")]/em/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon
        icon_url = res.select('.pic')[0].select('img')[0]['data-original']
        if icon_url and icon_url != 0:
            item['icon_url'] = icon_url
        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"down_pc cf")]/a/@href').extract())
        if apk_download and apk_download != 0:
            item['apk_download'] = apk_download
        # 大小
        size = ''.join(sel.xpath('//ul[contains(@class, "dd cf")]/li[3]/text()').extract()).split(u'：')[-1]
        if size and size != 0:
            item['size'] = size
        # 版本
        version = ''.join(sel.xpath('//ul[contains(@class, "dd cf")]/li[4]/text()').extract()).split(u'：')[-1]
        if version and version != 0:
            item['version'] = version
        # 評分次數
        download_number = ''.join(sel.xpath('//ul[contains(@class, "dd cf")]/li[1]/text()').extract()).split(u'：')[-1]
        if download_number and download_number != 0:
            item['download_number'] = download_number
        #開發者
        creator = res.select('.content')[0].select('.tab3')[1]['title']
        if creator and creator != 0:
            item['creator'] = creator
        return item

#lenovomm 已修改(（302）有package_name但沒給)
class lenovomm(scrapy.Spider):
    name ='lenovomm'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.lenovomm.com"]  # 不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://www.lenovomm.com/index_app.html']#,'http://www.lenovomm.com/index_game.html'

    def __init__(self, project_id=''):
        super(lenovomm, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結第1個為全部，從第2個類開始[1:]
        for category in sel.xpath('//ul[contains(@class,"typesList f12 fl mt10")]/li/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})  #
            # print '>>>>>>111>>>>>>>>>' + category
    def parse_category(self,response):
        sel = Selector(response)
        url = response.url
        item = ApkcrawlerItem()
        # 取apk的連結-->進入parse_apkDetail截取apk內容資料
        for apk_link in sel.xpath('//ul[contains(@class,"apps")]/li/div/a[2]/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail, headers={'Referer': url})  #,meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"pagenav tcenter")]/a/@href')[-1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category, headers={'Referer': url})
            # print '>>>>>>next_page>>>>>>>>>' +next_page
    def parse_apkDetail(self,response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        #38~53為print出
        # package_name = response.url.split(u'appdetail/')[-1].split(u'/0')[0]
        # print package_name
        # category = ''.join(sel.xpath('//div[contains(@class,"w1000 bcenter")]/a[3]/text()').extract())
        # print category #分類
        # name = ''.join(sel.xpath('//div[contains(@class,"ff-wryh detailAppname txtCut")]/h1/text()').extract())
        # print name #名稱
        # icon = ''.join(sel.xpath('//div[contains(@class,"detailIcon pr fl")]/img/@src').extract())
        # print icon #icon_url
        # apk_download = ''.join(sel.xpath('//*[@id="downAPK"]/@href').extract())
        # print apk_download  # 下載
        # size = ''.join(sel.xpath('//ul[contains(@class,"detailAppInfo fl")]/li[1]/span/text()').extract())
        # print size #大小
        # version = ''.join(sel.xpath('//ul[contains(@class,"detailAppInfo fl")]/li[2]/span/text()').extract())
        # print version #版本
        # download_number = ''.join(sel.xpath('//div[contains(@class,"f12 detailDownNum cb clearfix")]/span/text()').extract()).split(u'下载：')[-1].split(u'次')[0]
        # print download_number #下載次數
        # creator = ''.join(sel.xpath('//ul[contains(@class,"detailAppInfo fl")]/li[4]/span/text()').extract())
        # print creator #開發者
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'lenovomm'
        # item['category'] = 'zero'
        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['package_name'] = 'zero' #無package name
        item['creator'] = 'zero'
        #分類
        # category = ''.join(sel.xpath('//div[contains(@class,"w1000 bcenter")]/a[3]/text()').extract())
        # if category and category != 0:
        #     item['category'] = category
        #package_name
        package_name = response.url.split(u'appdetail/')[-1].split(u'/0')[0]
        if package_name and package_name != 0:
            item['package_name'] = package_name
        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"ff-wryh detailAppname txtCut")]/h1/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon_url
        icon_url = ''.join(sel.xpath('//div[contains(@class,"detailIcon pr fl")]/img/@src').extract())
        if icon_url and icon_url != 0:
            item['icon_url'] = icon_url
        # 下載
        apk_download = ''.join(sel.xpath('//*[@id="downAPK"]/@href').extract())
        if apk_download and apk_download != 0:
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download
        # 大小
        size = ''.join(sel.xpath('//ul[contains(@class,"detailAppInfo fl")]/li[1]/span/text()').extract())
        if size and size != 0:
            item['size'] = size
        # 版本
        version = ''.join(sel.xpath('//ul[contains(@class,"detailAppInfo fl")]/li[2]/span/text()').extract())
        if version and version != 0:
            item['version'] = version
        #評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"f12 detailDownNum cb clearfix")]/span/text()').extract()).split(u'下载：')[-1].split(u'次')[0]
        if download_number and download_number !=0:
            item['download_number'] = download_number
        #開發者
        creator = ''.join(sel.xpath('//ul[contains(@class,"detailAppInfo fl")]/li[4]/span/text()').extract())
        if creator and creator != 0:
            item['creator']=creator
        return item


# liqucn
class liqucn(scrapy.Spider):
    name ='liqucn'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["os-android.liqucn.com"]  # 不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://os-android.liqucn.com/rj/','http://os-android.liqucn.com/yx/','http://os-android.liqucn.com/wy/']#

    def __init__(self, project_id=''):
        super(liqucn, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結第1個為全部，從第2個類開始[1:]
        for category in sel.xpath('//div[contains(@class,"rj_sift")]/dl[1]/dd/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})  #
            # print '>>>>>>111>>>>>>>>>' + category
    def parse_category(self,response):
        sel = Selector(response)
        # res = BeautifulSoup(response.body)
        url = response.url
        # print url
        # 取apk的連結-->進入parse_apkDetail截取apk內容資料
        for apk_link in sel.xpath('//ul[contains(@class,"app_list")]/li/div/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail, headers={'Referer': url})
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁(下一頁位於倒數第二個位置)
        for next_page in sel.xpath('//div[contains(@class,"page")]/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_category,headers={'Referer': url})
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁(下一頁位於倒數第一個位置)
        for next_page in sel.xpath('//div[contains(@class,"page")]/a')[-1:]:
            next_page_text01 = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text01
            if u'下一页' in next_page_text01:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_category,headers={'Referer': url})
    def parse_apkDetail(self,response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        #51~67為print出
        # category = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/p[1]/em[1]/text()').extract())
        # print category #分類
        # name = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/h1/text()').extract())
        # print name #名稱
        # icon = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/img/@src').extract())
        # print icon #icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"btn_list")]/a/@href').extract())
        # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
        # print apk_download  # 下載
        # size = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/p[2]/em[2]/text()').extract())
        # print size #大小
        # version = ''.join(sel.xpath('//div[contains(@class,"section")]/p/text()[2]').extract()).split(u'最新版：')[1]
        # print version #版本
        # download_number = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/p[2]/em[3]/text()').extract())
        # print download_number #下載次數
        # creator = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/p[1]/em[3]/text()').extract())
        # print creator #開發者
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'liqucn'
        # item['category'] = 'zero'
        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['package_name'] = 'zero' #無package name
        item['creator'] = 'zero'
        #分類
        # category = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/p[1]/em[1]/text()').extract())
        # if category and category != 0:
        #     item['category'] = category
        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/h1/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon_url
        icon_url = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/img/@src').extract())
        if icon_url and icon_url != 0:
            item['icon_url'] = icon_url
        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"btn_list")]/a/@href').extract())
        if apk_download and apk_download != 0:
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download
        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/p[2]/em[2]/text()').extract())
        if size and size != 0:
            item['size'] = size
        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"section")]/p/text()[2]').extract()).split(u'最新版：')[1]
        if version and version != 0:
            item['version'] = version
        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/p[2]/em[3]/text()').extract())
        if download_number and download_number != 0:
            item['download_number'] = download_number
        #開發者
        creator = ''.join(sel.xpath('//div[contains(@class,"app_leftinfo")]/p[1]/em[3]/text()').extract())
        if creator and creator != 0:
            item['creator']=creator
        return item


# yayawan ('回合战斗'apk沒載點)
class yayawan(scrapy.Spider):
    name = "yayawan"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.yayawan.com"]
    start_urls = ['http://www.yayawan.com/game']#只有game

    def __init__(self, project_id=''):
        super(yayawan, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/category-hhzd-pos-new)從第一個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"main_left_top")]/div[1]/div[2]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link,headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/game/yywcs/)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"contxt_row_r1")]/h3/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})  #
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        # for next_page in sel.xpath('//a[contains(@class,"tsp_next")]/@href').extract():
        #     print '>>>>>>next_page>>>>>>>>>' + next_page
        #     yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_apk_link,headers={'Referer': url})

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"pages")]/form/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"game_info over")]/div/h1/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"game_info over")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//p[contains(@class,"downbtn")]/a[3]/@ex_url').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"info apk")]/p[1]/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"info apk")]/p[2]/text()').extract()).split(u'：')[-1]  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"game_info over")]/div/p[2]/text()').extract()).split(u'：')[-1].split(u' ')[0].replace('\n','')  # 評分次數
        # print download_number
        # creator = 'zero'  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'yayawan'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"game_info over")]/div/h1/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"game_info over")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//p[contains(@class,"downbtn")]/a[3]/@ex_url').extract())
        if apk_download and apk_download != '0':
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"info apk")]/p[1]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"info apk")]/p[2]/text()').extract()).split(u'：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"game_info over")]/div/p[2]/text()').extract()).split(u'：')[-1].split(u' ')[0].replace('\n','')
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者(沒有)

        return item


# mi (軟件＆遊戲可以在同一組網址擷取連結；下一頁是.js，用API解js)
class mi(scrapy.Spider):
    name = "mi"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["app.mi.com"]
    start_urls = ['http://app.mi.com']#軟件＆遊戲可以在同一組網址擷取連結

    def __init__(self, project_id=''):
        super(mi, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url

        # (第一層)取分類的連結(/category/5)，從第一個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//ul[contains(@class,"category-list")]/li/a/@href')[0:].extract():#[-2:-1]是測試/category/28（頁數最少-共23頁）
            # category_number = category.split(u'/category/')[-1]
            # print '>>>>>>category_number>>>>>>>>>' + category_number
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_page_one,headers={'Referer': url})

    def parse_page_one(self, response):
        url = response.url
        # print '>>>>>>parse_page>>>>>>>>>' + url
        page_url_1 = 'http://app.mi.com/categotyAllListApi?page='
        page_url_2 = '&categoryId='
        page_url_3 = '&pageSize=30'
        category_number = url.split(u'/')[-1]
        page_number = 0
        yield scrapy.Request(page_url_1 + str(page_number) + page_url_2 + str(category_number) + page_url_3,callback=self.parse_apk_link, headers={'Referer': url})  #

    def parse_apk_link(self, response):
        url = response.url
        # print '>>>>>>parse_apk_link>>>>>>>>>' + url

        json_response = json.loads(response.body)['data']
        # print json_response[0:2]#每頁共30筆
        for json_data in json_response[0:]:
            # print str(json_data['appId'])
            apk_link_url = 'detail/' + str(json_data['appId'])
            # print apk_link_url
            yield scrapy.Request(urlparse.urljoin(url, apk_link_url), self.parse_apkDetail, headers={'Referer': url})  #

        # if 'appId' in str(json_response):
        #     print '::::::::::::::::::::good::::::::::::::::::::::'
        #     page_url_1 = 'http://app.mi.com/categotyAllListApi?page='
        #     page_url_2 = '&categoryId='+url.split('&categoryId=')[-1].split('&pageSize=30')[0]
        #     page_url_3 = '&pageSize=30'
        #     page_number = int(url.split('page=')[-1].split('&')[0])+1
        #     print page_number
        #     yield scrapy.Request(page_url_1 + str(page_number) + page_url_2 + page_url_3,callback=self.parse_apk_link, headers={'Referer': url})  #
            # page_number+=1

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = ''.join(sel.xpath('//div[contains(@class,"details preventDefault")]/ul[1]/li[8]/text()').extract())
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"intro-titles")]/h3/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"app-info")][1]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = 'http://app.mi.com'+''.join(sel.xpath('//div[contains(@class,"app-info-down")]/a/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"details preventDefault")]/ul[1]/li[2]/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"details preventDefault")]/ul[1]/li[4]/text()').extract())  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"intro-titles")]/span[1]/text()').extract()).strip(u'(').strip(u' ').split(u'次')[0]  # 評分次數
        # print download_number
        # creator = ''.join(sel.xpath('//div[contains(@class,"intro-titles")]/p[1]/text()').extract())  # 開發者
        # print creator

        item = ApkcrawlerItem()
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'mi'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)
        package_name = ''.join(sel.xpath('//div[contains(@class,"details preventDefault")]/ul[1]/li[8]/text()').extract())
        if package_name and package_name != '0':
            item['package_name'] = package_name  # package_name

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"intro-titles")]/h3/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"app-info")][1]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = 'http://app.mi.com'+''.join(sel.xpath('//div[contains(@class,"app-info-down")]/a/@href').extract())
        if apk_download and apk_download != '0':
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"details preventDefault")]/ul[1]/li[2]/text()').extract())
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"details preventDefault")]/ul[1]/li[4]/text()').extract())
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"intro-titles")]/span[1]/text()').extract()).strip(u'(').strip(u' ').split(u'次')[0]
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者(沒有)
        creator = ''.join(sel.xpath('//div[contains(@class,"intro-titles")]/p[1]/text()').extract())
        if creator and creator != '0':
            item['creator'] = creator  # 名稱

        return item


# greenxiazai (無分類，軟件＆遊戲；)
class greenxiazai(scrapy.Spider):
    name = "greenxiazai"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.greenxiazai.com"]
    start_urls = ['http://www.greenxiazai.com/soft/greenxiazai-166-1.html','http://www.greenxiazai.com/soft/greenxiazai-172-1.html']#

    def __init__(self, project_id=''):
        super(greenxiazai, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第一層)取apk的連結(/game/yywcs/)-->進入第二層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"span-18 last")]/div[2]/div[1]/div/h2/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})  #
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse動作，直到沒下頁
        # for next_page in sel.xpath('//a[contains(@class,"tsp_next")]/@href').extract():
        #     print '>>>>>>next_page>>>>>>>>>' + next_page
        #     yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_apk_link,headers={'Referer': url})

        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"dede_pages")]/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'  # package_name
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"sif_l")]/ul[1]/li[1]/a/text()').extract())  # 名稱
        # print name
        # icon_url = 'zero'  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//li[contains(@class,"ico_pt")]/a/@href').extract()[-1:])  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"sif_l")]/ul[1]/li[3]/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"tit")]/div[1]/h1/text()').extract()).split(u'v')[-1]  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//span[contains(@class,"showscore1")]/text()').extract())  # 評分次數
        # print download_number
        # creator = 'zero'  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'greenxiazai'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"sif_l")]/ul[1]/li[1]/a/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon(沒有)

        # 下載
        apk_download = ''.join(sel.xpath('//li[contains(@class,"ico_pt")]/a/@href').extract()[-1:])
        if apk_download and apk_download != '0':
            # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"sif_l")]/ul[1]/li[3]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"tit")]/div[1]/h1/text()').extract()).split(u'v')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//span[contains(@class,"showscore1")]/text()').extract())
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者(沒有)

        return item


# souha (apk不多，沒有'下一頁‘)
class souha(scrapy.Spider):
    name = "souha"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.souha.net"]
    start_urls = ['http://www.souha.net/e/action/ListInfo/?classid=7','http://www.souha.net/e/action/ListInfo/?classid=8']#

    def __init__(self, project_id=''):
        super(souha, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/e/action/ListInfo/?classid=13)，第一個是'全部'，從第二個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"mod-box mod-box-padding")]/nav[1]/ul/li/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link,headers={'Referer': url})  #
            print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/android/653.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"mod-box mod-box-padding")]/section/div[2]/ul/li/div[1]/div[1]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})  #
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
    # 沒有'下一頁'
    #     # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
    #     # for next_page in sel.xpath('//a[contains(@class,"tsp_next")]/@href').extract():
    #     #     print '>>>>>>next_page>>>>>>>>>' + next_page
    #     #     yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_apk_link,headers={'Referer': url})
    #
    #     # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"pages")]/form/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})
    #
    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/h1/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"app-img")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = 'http://www.souha.net'+''.join(sel.xpath('//div[contains(@class,"installbtn")]/a[1]/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/dl[1]/dd[4]/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/dl[1]/dd[1]/text()').extract())  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/dl[1]/dd[5]/text()').extract())  # 評分次數
        # print download_number
        # creator = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/dl[1]/dd[8]/text()').extract())  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'souha'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/h1/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"app-img")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = 'http://www.souha.net'+''.join(sel.xpath('//div[contains(@class,"installbtn")]/a[1]/@href').extract())
        if apk_download and apk_download != '0':
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/dl[1]/dd[4]/text()').extract())
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/dl[1]/dd[1]/text()').extract())
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"app-msg")]/dl[1]/dd[5]/text()').extract())
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者(沒有)

        return item

# mm10086 (有付費的；apk頁面是js'正在加载... '；轉址,06/27)
class mm10086(scrapy.Spider):
    name = "mm10086"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["mm.10086.cn"]
    start_urls = ['http://mm.10086.cn/android/game?pay=1','http://mm.10086.cn/android/software?pay=1','http://mm.10086.cn/android/theme?pay=1']#

    def __init__(self, project_id=''):
        super(mm10086, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/android/game/dzmx?pay=1)，第一個是'全部'，從第二個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"nav clearfix mt15")]/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_page_change,headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_page_change(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>parse_page_change>>>>>>>>>' + url

        allpage = ''.join(sel.xpath('//a[contains(@class,"last")]/@href').extract()).split(u'&p=')[-1]#取總頁數int
        # print '>>>>>>allpage>>>>>>>>>' + allpage

        # 轉換url(page(總頁數) & screen(4))
        for allpage_number in range(1,int(allpage)+1):#int(allpage)+1
            for screen_number in range(1,5):
                change_page_url = url+'&order=9&p=' + str(allpage_number) + '&screen=' + str(screen_number)
                # print change_page_url
                yield scrapy.Request(change_page_url, callback=self.parse_apk_link, headers={'Referer': change_page_url})  #

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()

        # (第二層)取apk的連結(/android/653.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"part-1")]/div[1]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})  #
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"mj_big_title font-f-yh")]/span/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"mj_left_tu")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"mj_cont_left_t")]/a[1]/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"mj_info font-f-yh")]/ul[1]/li[4]/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"mj_info font-f-yh")]/ul[1]/li[3]/text()').extract()).split(u'：')[-1]  # 版本
        # print version
        # download_number = 'zero'  # 評分次數
        # print download_number
        # creator = ''.join(sel.xpath('//div[contains(@class,"mj_info font-f-yh")]/ul[1]/li[5]/a/@title').extract()).split(u'：')[-1]  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'mm10086'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"mj_big_title font-f-yh")]/span/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"mj_left_tu")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"mj_cont_left_t")]/a[1]/@href').extract())
        if apk_download and apk_download != '0':
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"mj_info font-f-yh")]/ul[1]/li[4]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"mj_info font-f-yh")]/ul[1]/li[3]/text()').extract()).split(u'：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數(沒有)

        # 開發者
        creator = ''.join(sel.xpath('//div[contains(@class,"mj_info font-f-yh")]/ul[1]/li[5]/a/@title').extract()).split(u'：')[-1]
        if creator and creator != '0':
            item['creator'] = creator  # 評分次數
        return item


# lvseba (android跟ios沒區分，多寫一層parse_Android_Check)
class lvseba(scrapy.Spider):
    name = "lvseba"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.lvseba.com"]
    start_urls = ['http://www.lvseba.com/shouji/tushudongman/']#只有一個

    def __init__(self, project_id=''):
        super(lvseba, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/shouji/tushudongman/)，倒數第二個是iphone的，從第一個類別開始到倒數第二個 ; -->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@class,"sideBox catalog")]/div[2]/ul/li/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link,headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/shouji/tushudongman/56084.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//ul[contains(@class,"software")]/li/h4/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_Android_Check,headers={'Referer': url})  #, meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"pageBox")][2]/div/ul/li/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})

    def parse_Android_Check(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>parse_Android_Check>>>>>>>>>' + url
        if u'Android' in ''.join(sel.xpath('//div[contains(@class,"info left")]/dl/dd[2]/span/text()').extract()):
            yield scrapy.Request(url, callback=self.parse_apkDetail,headers={'Referer': url},dont_filter=True)


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"box box2 detail-info")]/div[1]/div[1]/h3/text()').extract()).split(u' ')[0]  # 名稱
        # print name
        # icon_url = 'zero'  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"dl-bd")]/ul/li/a[1]/@href')[0:1].extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"info left")]/dl/dt[1]/span/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"box box2 detail-info")]/div[1]/div[1]/h3/text()').extract()).split(u'v')[-1].split(u' ')[0]  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"digg_num digg_good left")]/a/text()').extract()).split(u'：')[-1]  # 評分次數
        # print download_number
        # creator = ''.join(sel.xpath('//div[contains(@class,"info left")]/dl/dd[1]/span/text()').extract())  # 開發者
        # print creator

        item = ApkcrawlerItem()
        # item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'lvseba'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"box box2 detail-info")]/div[1]/div[1]/h3/text()').extract()).split(u' ')[0]
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon(沒有)

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"dl-bd")]/ul/li/a[1]/@href')[0:1].extract())
        if apk_download and apk_download != '0':
            # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"info left")]/dl/dt[1]/span/text()').extract())
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"box box2 detail-info")]/div[1]/div[1]/h3/text()').extract()).split(u'v')[-1].split(u' ')[0]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"digg_num digg_good left")]/a/text()').extract()).split(u'：')[-1]
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者
        creator = ''.join(sel.xpath('//div[contains(@class,"info left")]/dl/dd[1]/span/text()').extract())
        if creator and creator != '0':
            item['creator'] = creator  # 評分次數

        return item

# anzhi (403；apk download - javascript)
class anzhi(scrapy.Spider):
    name = "anzhi"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.anzhi.com"]
    start_urls = ['http://www.anzhi.com/list_1_1_new.html','http://www.anzhi.com/list_2_1_new.html']#

    def __init__(self, project_id=''):
        super(anzhi, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/soft_2642788.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"app_icon")]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url})  #, meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//a[contains(@class,"next")]'):
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'>' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse,headers={'Referer': url})

    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = ''.join(sel.xpath('//div[contains(@class,"detail_icon")]/ul/li[2]/a/@href').extract()).split(u'package=')[-1]  # package_name
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"detail_description")]/div[1]/h3/text()').extract())  # 名稱
        # print name
        # icon_url = 'http://www.anzhi.com'+''.join(sel.xpath('//div[contains(@class,"detail_icon")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = 'http://www.anzhi.com/dl_app.php?s='+''.join(sel.xpath('//div[contains(@class,"detail_down")]/a/@onclick').extract()).split(u'(')[-1].split(u')')[0]+'&n=5'  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//ul[contains(@id,"detail_line_ul")]/li[4]/span/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//ul[contains(@id,"detail_line_ul")]/li[5]/text()').extract()).split(u'：')[-1]  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//ul[contains(@id,"detail_line_ul")]/li[2]/span/text()').extract()).split(u'：')[-1]  # 評分次數
        # print download_number
        # creator = ''.join(sel.xpath('//ul[contains(@id,"detail_line_ul")]/li[7]/text()').extract()).split(u'：')[-1]  # 开发者
        # print creator

        item = ApkcrawlerItem()
        # item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'anzhi'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name
        package_name = ''.join(sel.xpath('//div[contains(@class,"detail_icon")]/ul/li[2]/a/@href').extract()).split(u'package=')[-1]
        if package_name and package_name != '0':
            item['package_name'] = package_name  # 名稱

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"detail_description")]/div[1]/h3/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = 'http://www.anzhi.com'+''.join(sel.xpath('//div[contains(@class,"detail_icon")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = 'http://www.anzhi.com/dl_app.php?s='+''.join(sel.xpath('//div[contains(@class,"detail_down")]/a/@onclick').extract()).split(u'(')[-1].split(u')')[0]+'&n=5'
        if apk_download and apk_download != '0':
            req = urllib2.Request(apk_download, headers={'User-Agent': "Magic Browser"})
            apk_download = urllib2.urlopen(req, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//ul[contains(@id,"detail_line_ul")]/li[4]/span/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//ul[contains(@id,"detail_line_ul")]/li[5]/text()').extract()).split(u'：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//ul[contains(@id,"detail_line_ul")]/li[2]/span/text()').extract()).split(u'：')[-1]
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 評分次數

        # 開發者
        creator = ''.join(sel.xpath('//ul[contains(@id,"detail_line_ul")]/li[7]/text()').extract()).split(u'：')[-1]
        if creator and creator != '0':
            item['creator'] = creator  # 評分次數

        return item


class app9553(scrapy.Spider):
    name ='app9553'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.9553.com"]  # 不能加http://，遇到Filtered offsite request連www.都拿掉
    start_urls = ['http://www.9553.com/android/soft.htm','http://www.9553.com/android/game.htm']#

    def __init__(self, project_id=''):
        super(app9553, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # # (第一層)取分類的連結
        if 'soft' in url:
            for category in sel.xpath('//nav[contains(@class,"nav mg-t15 clear")]/ul/li[2]/p/a/@href')[0:].extract():
                yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})  #
                # print '>>>>>>111>>>>>>>>>' + category
        elif 'game' in url:
            for category in sel.xpath('//nav[contains(@class,"nav mg-t15 clear")]/ul/li[3]/p/a/@href')[0:].extract():
                yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})  #
                # print '>>>>>>111>>>>>>>>>' + category
    def parse_category(self,response):
        sel = Selector(response)
        url = response.url
        # 取apk的連結-->進入parse_apkDetail截取apk內容資料
        for apk_link in sel.xpath('//ul[contains(@class,"list-bd")]/li/a[1]/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail, headers={'Referer': url})  #,meta={'item': item}
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁
        if u'下一页' in ''.join(sel.xpath('//div[contains(@class,"page-bd")]/a/text()')[-1:].extract()):
            for next_page in sel.xpath('//div[contains(@class,"page-bd")]/a/@href')[-1:].extract():
                yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category, headers={'Referer': url})
                # print '>>>>>>next_page>>>>>>>>>' +next_page
    def parse_apkDetail(self,response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        #42~56為print出
        # category = ''.join(sel.xpath('//ul[contains(@class,"mod-list zm")]/li[1]/text()').extract()).split(u'：')[-1]
        # print category #分類
        # name = ''.join(sel.xpath('//div[contains(@class,"w543 fl-rt")]/h1/text()').extract())
        # print name #名稱
        # icon = ''.join(sel.xpath('//div[contains(@class,"w105 fl-lf")]/p[1]/img/@src').extract())
        # print icon #icon_url
        # apk_download = str(''.join(sel.xpath('//div[contains(@class,"w300 fl-lf tab-bd")]/script').extract()).encode('utf8'))
        # apk_download = apk_download.split('var realdown = "')[-1].split('";')[0]
        # print apk_download  # 下載
        # size = ''.join(sel.xpath('//ul[contains(@class,"mod-list zm")]/li[4]/text()').extract()).split(u'：')[-1]
        # print size #大小
        # version = ''.join(sel.xpath('//ul[contains(@class,"mod-list zm")]/li[2]/text()').extract()).split(u'：')[-1]
        # print version #版本
        # download_number = ''.join(sel.xpath('//*[@id="score"]/text()').extract())
        # print download_number #評分
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'app9553'
        # item['category'] = 'zero'
        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['package_name'] = 'zero' #無
        item['creator'] = 'zero' #無
        #分類
        # category = ''.join(sel.xpath('//ul[contains(@class,"mod-list zm")]/li[1]/text()').extract()).split(u'：')[-1]
        # if category and category != 0:
        #     item['category'] = category
        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"w543 fl-rt")]/h1/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon_url
        icon_url = ''.join(sel.xpath('//div[contains(@class,"w105 fl-lf")]/p[1]/img/@src').extract())
        if icon_url and icon_url != 0:
            item['icon_url'] = icon_url
        # # 下載
        apk_download = str(''.join(sel.xpath('//div[contains(@class,"w300 fl-lf tab-bd")]/script').extract()).encode('utf8'))
        apk_download = apk_download.split('var realdown = "')[-1].split('";')[0]
        if apk_download and apk_download != 0:
            item['apk_download'] = apk_download
        # 大小
        size = ''.join(sel.xpath('//ul[contains(@class,"mod-list zm")]/li[4]/text()').extract()).split(u'：')[-1]
        if size and size != 0:
            item['size'] = size
        # 版本
        version = ''.join(sel.xpath('//ul[contains(@class,"mod-list zm")]/li[2]/text()').extract()).split(u'：')[-1]
        if version and version != 0:
            item['version'] = version
        #評分次數
        download_number = ''.join(sel.xpath('//*[@id="score"]/text()').extract())
        if download_number and download_number !=0:
            item['download_number'] = download_number
        return item

class coolapk(scrapy.Spider):
    name ='coolapk'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.coolapk.com"]
    start_urls = ['http://www.coolapk.com/apk/','http://www.coolapk.com/game/']#

    def __init__(self, project_id=''):
        super(coolapk, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結第1個為全部，從第2個類開始[1:]
        for category in sel.xpath('//div[contains(@class,"col-md-4 ex-flex-col hidden-sm hidden-xs")]/div[1]/div[2]/ul/li/div/h4/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})  #
            # print '>>>>>>111>>>>>>>>>' + category
    def parse_category(self,response):
        sel = Selector(response)
        url = response.url
        # print url
        # 取apk的連結-->進入parse_apkDetail截取apk內容資料
        for apk_link in sel.xpath('//ul[contains(@class,"media-list ex-card-app-list")]/li/div/h4/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail, headers={'Referer': url})
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁(下一頁位於倒數第二個位置)
        for next_page in sel.xpath('//ul[contains(@class,"pagination")]/li/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'>' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_category,headers={'Referer': url})
    def parse_apkDetail(self,response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        url = response.url
        #44~62為print出
        # package_name = url.split('/')[-1]
        # print package_name #package_name
        # category = ''.join(sel.xpath('//div[contains(@class,"ex-card-wrapper")]/a[1]/text()').extract())
        # print category #分類
        # #/html/body/div[2]/div/div[1]/div/h1/text()
        # name = ''.join(sel.xpath('//div[contains(@class,"media ex-page-topbar")]/div/h1/text()').extract())
        # print name #名稱
        # icon =  ''.join(sel.xpath('//div[contains(@class,"media ex-page-topbar")]/a/img/@src').extract())
        # print icon #icon_url
        # domain = 'http://www.coolapk.com'
        # apk_download = domain + str(''.join(sel.xpath('///html/body/script[1]').extract())).split('apkDownloadUrl = "')[-1].split('"')[0]
        # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
        # print apk_download  # 下載
        # size = ''.join(sel.xpath('//div[contains(@class,"media-intro ex-apk-view-intro")]/span[2]/text()').extract()).split(u'，')[0]
        # print size #大小
        # version = ''.join(sel.xpath('//div[contains(@class,"media ex-page-topbar")]/div/h1/small/text()').extract())
        # print version #版本
        # download_number = ''.join(sel.xpath('//div[contains(@class,"media-intro ex-apk-view-intro")]/span[1]/text()').extract()).split(u'，')[1]
        # print download_number #下載次數
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'coolapk'
        # item['category'] = 'zero'
        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['package_name'] = 'zero'
        item['creator'] = 'zero' #無
        #package_name
        package_name = url.split('/')[-1]
        if package_name and package_name != 0:
            item['package_name'] = package_name
        #分類
        # category = ''.join(sel.xpath('//div[contains(@class,"ex-card-wrapper")]/a[1]/text()').extract())
        # if category and category != 0:
        #     item['category'] = category
        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"media ex-page-topbar")]/div/h1/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon_url
        icon_url = ''.join(sel.xpath('//div[contains(@class,"media ex-page-topbar")]/a/img/@src').extract())
        if icon_url and icon_url != 0:
            item['icon_url'] = icon_url
        # 下載
        domain = 'http://www.coolapk.com'
        apk_download = domain + str(''.join(sel.xpath('///html/body/script[1]').extract())).split('apkDownloadUrl = "')[-1].split('"')[0]
        if apk_download and apk_download != 0:
            apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download
        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"media-intro ex-apk-view-intro")]/span[2]/text()').extract()).split(u'，')[0]
        if size and size != 0:
            item['size'] = size
        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"media ex-page-topbar")]/div/h1/small/text()').extract())
        if version and version != 0:
            item['version'] = version
        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"media-intro ex-apk-view-intro")]/span[1]/text()').extract()).split(u'，')[1]
        if download_number and download_number != 0:
            item['download_number'] = download_number
        return item

#s5577 （部分載點未開放；(301)；(302)）
class s5577(scrapy.Spider):
    name ='s5577'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.5577.com"]
    start_urls = ['http://www.5577.com/games/r_2_1.html','http://www.5577.com/a/r_4_1.html','http://www.5577.com/a/r_1_1.html']#

    def __init__(self, project_id=''):
        super(s5577, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結第1個為全部，從第2個類開始[1:]
        for category in sel.xpath('//ul[contains(@class,"f-list f-fllist")]/li/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})  #
            # print '>>>>>>111>>>>>>>>>' + category
    def parse_category(self,response):
        sel = Selector(response)
        url = response.url
        # print url
        # 取apk的連結-->進入parse_apkDetail截取apk內容資料
        for apk_link in sel.xpath('//ul[contains(@class,"m-softlist")]/li/p[1]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail, headers={'Referer': url})
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁(下一頁位於倒數第二個位置)
        for next_page in sel.xpath('//div[contains(@class,"tsp_nav")]/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_category,headers={'Referer': url})
    def parse_apkDetail(self,response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        url = response.url
        # #41~65為print出
        # category = ''.join(sel.xpath('//p[contains(@class,"mbx")]/a[3]/text()').extract())
        # print category #分類
        # name = ''.join(sel.xpath('//dl[contains(@class,"m-softinfo")]/dt/h1/text()').extract())
        # print name #名稱
        # icon =  ''.join(sel.xpath('//dl[contains(@class,"m-softinfo")]/dt/img/@src').extract())
        # print icon #icon_url
        # if u'立即下载' in ''.join(sel.xpath('//ul[contains(@class,"btn")]/li[1]/a/text()').extract()):
        #     apk_download = ''.join(sel.xpath('//ul[contains(@class,"btn")]/li[1]/a/@href').extract())
        #     package_name = apk_download.split(u'5577.')[-1].split(u'.apk')[0]
        #     print apk_download # 下載
        #     print package_name # package_name
        # else:
        #     apk_download = ''.join(sel.xpath('//ul[contains(@class,"btn")]/li[2]/a/@href').extract())
        #     package_name = apk_download.split(u'5577.')[-1].split(u'.apk')[0]
        #     print apk_download  # 下載
        #     print package_name  # package_name
        # size = ''.join(sel.xpath('//div[contains(@class,"soft-info")]/ul[1]/li[1]/text()').extract())
        # print size #大小
        # version = ''.join(sel.xpath('//dl[contains(@class,"m-softinfo")]/dt/h1/em/text()').extract())
        # print version #版本
        # download_number = ''.join(sel.xpath('//div[contains(@class,"soft-info")]/ul[1]/li[3]/img/@src').extract()).split(u'star')[-1].split(u'.png')[0]
        # print download_number #評分次數
        # creator = ''.join(sel.xpath('//div[contains(@class,"soft-info")]/ul[1]/li[4]/a/span/text()').extract())
        # print creator
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 's5577'
        # item['category'] = 'zero'
        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['package_name'] = 'zero'
        item['creator'] = 'zero'
        #分類
        # category = ''.join(sel.xpath('//p[contains(@class,"mbx")]/a[3]/text()').extract())
        # if category and category != 0:
        #     item['category'] = category
        # 名稱
        name = ''.join(sel.xpath('//dl[contains(@class,"m-softinfo")]/dt/h1/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon_url
        icon_url = ''.join(sel.xpath('//dl[contains(@class,"m-softinfo")]/dt/img/@src').extract())
        if icon_url and icon_url != 0:
            item['icon_url'] = icon_url
        # 下載 & package_name
        if u'立即下载' in ''.join(sel.xpath('//ul[contains(@class,"btn")]/li[1]/a/text()').extract()):
            apk_download = ''.join(sel.xpath('//ul[contains(@class,"btn")]/li[1]/a/@href').extract())
            if apk_download and apk_download != 0:
                item['apk_download'] = apk_download
                item['package_name'] = apk_download.split(u'5577.')[-1].split(u'.apk')[0]
        else:
            apk_download = ''.join(sel.xpath('//ul[contains(@class,"btn")]/li[2]/a/@href').extract())
            if apk_download and apk_download != 0:
                item['apk_download'] = apk_download
                item['package_name'] = apk_download.split(u'5577.')[-1].split(u'.apk')[0]
        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"soft-info")]/ul[1]/li[1]/text()').extract())
        if size and size != 0:
            item['size'] = size
        # 版本
        version = ''.join(sel.xpath('//dl[contains(@class,"m-softinfo")]/dt/h1/em/text()').extract())
        if version and version != 0:
            item['version'] = version
        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"soft-info")]/ul[1]/li[3]/img/@src').extract()).split(u'star')[-1].split(u'.png')[0]
        if download_number and download_number != 0:
            item['download_number'] = download_number
        #開發者
        creator = ''.join(sel.xpath('//div[contains(@class,"soft-info")]/ul[1]/li[4]/a/span/text()').extract())
        if creator and creator != 0:
            item['creator'] = creator
        return item


# newsasp (分類有做處理須看程式碼上的註解；packagename有在原始碼內但每個app都一樣；version沒有但名稱內有用名城代替)
class newsasp(scrapy.Spider):
    name ='newsasp'
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.newasp.net"]
    start_urls = ['http://www.newasp.net/android/list-469-1.html','http://www.newasp.net/android/list-430-1.html','http://www.newasp.net/android/list-470-1.html']#

    def __init__(self, project_id=''):
        super(newsasp, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # 計算類別總個數
        # 減13是為了要拿掉国产软件、国外软件、汉化补丁、免费软件、试用软件、共享软件、商业软件、特别软件、绿色软件、简体中文、繁体中文、英文、多国语言
        # 這些是細項在篩選跟類別在同一個class內所以要拿掉
        number=0
        for time in sel.xpath('//div[contains(@class,"lstcatbox")]/ul/li/a')[0:].extract():
            number = number + 1
        number = number - 13
        # (第一層)取分類的連結
        # 測試全部要用[0:number]
        for category in sel.xpath('//div[contains(@class,"lstcatbox")]/ul/li/a/@href')[0:number].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_category,headers={'Referer': url})  #
            # print '>>>>>>111>>>>>>>>>' + category
    def parse_category(self,response):
        sel = Selector(response)
        url = response.url
        # print url
        # 取apk的連結-->進入parse_apkDetail截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"soft_list_box")]/div/div[1]/h2/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail, headers={'Referer': url})
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link
        # # # (第一層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列category動作，直到沒下頁(下一頁位於倒數第二個位置)
        for next_page in sel.xpath('//div[contains(@class,"pagebox")]/p/a')[-1:]:
            next_text = ''.join(next_page.xpath('.//em/text()').extract())
            if u'下一页' in next_text:
                next_page = ''.join(next_page.xpath('.//@href').extract())
                yield scrapy.Request(urlparse.urljoin(url, next_page), callback=self.parse_category,headers={'Referer': url})
                # print '>>>>>>next_page>>>>>>>>>' + next_page
    def parse_apkDetail(self,response):
        item = ApkcrawlerItem()
        sel = Selector(response)
        url = response.url
        res = BeautifulSoup(response.body)
        # #41~65為print出
        # category = ''.join(sel.xpath('//div[contains(@class,"crumb")]/a[3]/text()').extract())
        # print category #分類
        # name = ''.join(sel.xpath('//div[contains(@class,"tit")]/h1/text()').extract())
        # print name #名稱
        # icon =  ''.join(sel.xpath('//div[contains(@class,"imgbox")]/a/img/@src').extract())
        # print icon #icon_url
        # apk_download = ''.join(sel.xpath('//*[@id="downlist"]').extract()).split(u'">本地联通下载')[0].split(u'href="')[-1]
        # print apk_download # 下載
        # size = ''.join(sel.xpath('//ul[contains(@class,"infolist")]/li[4]/span/text()').extract())
        # print size #大小
        # version = ''.join(sel.xpath('//div[contains(@class,"tit")]/h1/text()').extract())
        # print version #無版本使用名稱(名稱內有版本)
        # download_number = ''.join(sel.xpath('//ul[contains(@class,"infolist")]/li[5]/img/@src').extract()).split(u'star')[-1].split(u'.gif')[0]
        # print download_number #評分次數
        # creator = ''.join(sel.xpath('//ul[contains(@class,"infolist")]/li[6]/a/text()').extract())
        # if creator:
        #     print creator
        # else:
        #     creator = ''.join(sel.xpath('//ul[contains(@class,"infolist")]/li[6]/text()').extract())
        #     print creator
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'newsasp'
        # item['category'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero' #名稱替代
        item['download_number'] = 'zero'
        item['package_name'] = 'zero' #無
        item['creator'] = 'zero'
        #分類
        # category = ''.join(sel.xpath('//div[contains(@class,"crumb")]/a[3]/text()').extract())
        # if category and category != 0:
        #     item['category'] = category
        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"tit")]/h1/text()').extract())
        if name and name != 0:
            item['name'] = name
        # icon_url
        icon_url = ''.join(sel.xpath('//div[contains(@class,"imgbox")]/a/img/@src').extract())
        if icon_url and icon_url != 0:
            item['icon_url'] = icon_url
        # 下載
        apk_download = ''.join(sel.xpath('//*[@id="downlist"]').extract()).split(u'">本地联通下载')[0].split(u'href="')[-1]
        if apk_download and apk_download != 0:
            item['apk_download'] = apk_download
        # 大小
        size = ''.join(sel.xpath('//ul[contains(@class,"infolist")]/li[4]/span/text()').extract())
        if size and size != 0:
            item['size'] = size
        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"tit")]/h1/text()').extract())
        if version and version != 0:
            item['version'] = version
        # 評分次數
        download_number = ''.join(sel.xpath('//ul[contains(@class,"infolist")]/li[5]/img/@src').extract()).split(u'star')[-1].split(u'.gif')[0]
        if download_number and download_number != 0:
            item['download_number'] = download_number
        #開發者
        creator = ''.join(sel.xpath('//ul[contains(@class,"infolist")]/li[6]/a/text()').extract())
        if creator and creator != 0:
            item['creator'] = creator
        else:
            creator = ''.join(sel.xpath('//ul[contains(@class,"infolist")]/li[6]/text()').extract())
            if creator and creator != 0:
                item['creator'] = creator
        return item


# app962 (只有遊戲)
class app962(scrapy.Spider):
    name = "app962"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["962.net"]
    start_urls = ['http://www.962.net/azgame/f_5_1.html']#

    def __init__(self, project_id=''):
        super(app962, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(http://962.net/azgame/g_48_1.html)，第一個是'全部'，從第二個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//dd[contains(@id,"navlink")]/a/@href')[1:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link,headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/android/653.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//dl[contains(@class,"list")]/dd/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})  #
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"tsp_nav")]/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('./i//text()').extract())
            print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"g-game-h1")]/h1/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//li[contains(@class,"g-inrodul-img")]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//ul[contains(@class,"m-down-ul")]/li[2]/a/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//ul[contains(@class,"g-game-introdul")]/li[3]/span/text()').extract())  # 大小
        # print size
        # version = ''.join(sel.xpath('//ul[contains(@class,"g-game-introdul")]/li[5]/span/text()').extract())  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@id,"s_comment")]/form/h2/a/i/text()').extract())  # 評分次數
        # print download_number
        # creator = 'zero'  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'app962'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"g-game-h1")]/h1/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//li[contains(@class,"g-inrodul-img")]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//ul[contains(@class,"m-down-ul")]/li[2]/a/@href').extract())
        if apk_download and apk_download != '0':
            # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//ul[contains(@class,"g-game-introdul")]/li[3]/span/text()').extract())
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//ul[contains(@class,"g-game-introdul")]/li[5]/span/text()').extract())
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@id,"s_comment")]/form/h2/a/i/text()').extract())
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 版本

        # 開發者(沒有)

        return item

# eoemarket ()
class eoemarket(scrapy.Spider):
    name = "eoemarket"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.eoemarket.com"]
    start_urls = ['http://www.eoemarket.com/soft/','http://www.eoemarket.com/game/']#

    def __init__(self, project_id=''):
        super(eoemarket, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/soft/8_hot_unofficial_hasad_1_1.html)，從第一個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//span[contains(@class,"classfy_h1")]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link,headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/android/653.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"classf_list_item_c")]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})  #
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"page_c_eoe")]/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"name_introduce ml20")]/span[1]/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@class,"appicon_intr")]/span/img/@src').extract())  # icon
        # print icon_url
        # apk_download = ''.join(sel.xpath('//div[contains(@class,"qrcode_introduce")]/span[1]/a/@href').extract())  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//span[contains(@class,"info_appintr clearfix")]/span[1]/em[1]/text()').extract()).split(u'：')[-1]  # 大小
        # print size
        # version = ''.join(sel.xpath('//span[contains(@class,"info_appintr clearfix")]/span[1]/em[2]/text()').extract()).split(u'：')[-1]  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//span[contains(@class,"info_appintr clearfix")]/span[1]/em[3]/text()').extract()).split(u'：')[-1]  # 評分次數
        # print download_number
        # creator = 'zero'  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'eoemarket'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"name_introduce ml20")]/span[1]/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@class,"appicon_intr")]/span/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = ''.join(sel.xpath('//div[contains(@class,"qrcode_introduce")]/span[1]/a/@href').extract())
        if apk_download and apk_download != '0':
            # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//span[contains(@class,"info_appintr clearfix")]/span[1]/em[1]/text()').extract()).split(u'：')[-1]
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//span[contains(@class,"info_appintr clearfix")]/span[1]/em[2]/text()').extract()).split(u'：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//span[contains(@class,"info_appintr clearfix")]/span[1]/em[3]/text()').extract()).split(u'：')[-1]
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 版本

        # 開發者(沒有)

        return item


# doyo (&nbsp;；download-js(.exe)；download找規則切出.apk)
class doyo(scrapy.Spider):
    name = "doyo"
    # download_delay = 10
    # COOKIES_ENABLED = True
    allowed_domains = ["www.doyo.cn"]
    start_urls = ['http://www.doyo.cn/shouji/list/100/1/1/1/1']#僅遊戲

    def __init__(self, project_id=''):
        super(doyo, self)
        self.project_id = project_id

    def parse(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>>>>>>>>>>'+url
        # (第一層)取分類的連結(/soft/8_hot_unofficial_hasad_1_1.html)，從第一個類別開始到最後一個;-->進入第二層(分類頁)
        for category in sel.xpath('//div[contains(@id,"selector")]/div[3]/div[2]/a/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, category), callback=self.parse_apk_link,headers={'Referer': url})  #
            # print '>>>>>>category>>>>>>>>>' + category

    def parse_apk_link(self, response):
        sel = Selector(response)
        url = response.url
        # print '>>>>>>apk_link_url>>>>>>>>>' + url
        item = ApkcrawlerItem()
        # (第二層)取apk的連結(/android/653.html)-->進入第三層 截取apk內容資料
        for apk_link in sel.xpath('//div[contains(@class,"game_list")]/div/a[1]/@href')[0:].extract():
            yield scrapy.Request(urlparse.urljoin(url, apk_link), callback=self.parse_apkDetail,headers={'Referer': url}, meta={'item': item})  #
            # print '>>>>>>apk_link>>>>>>>>>' + apk_link

        # (第二層)當前頁面所有apk的內容截取取完後前往下頁重複執行上列parse_apk_link動作，直到沒下頁
        for next_page in sel.xpath('//div[contains(@class,"change_page")]/a')[-2:-1]:
            next_page_text = ''.join(next_page.xpath('.//text()').extract())
            # print '>>>>>>next_page_text>>>>>>>>>' + next_page_text
            if u'下一页' in next_page_text:
                next_page_href = ''.join(next_page.xpath('.//@href').extract())
                # print '>>>>>>next_page_href>>>>>>>>>' + next_page_href
                yield scrapy.Request(urlparse.urljoin(url, next_page_href), callback=self.parse_apk_link,headers={'Referer': url})


    def parse_apkDetail(self, response):
        sel = Selector(response)
        # package_name = 'zero'
        # print package_name
        # name = ''.join(sel.xpath('//div[contains(@class,"gameName")]/h1/text()').extract())  # 名稱
        # print name
        # icon_url = ''.join(sel.xpath('//div[contains(@id,"gameBox")]/div[1]/img/@src').extract())  # icon
        # print icon_url
        # apk_download = 'http://apk.doyo.cn/mobile/apk'+''.join(sel.xpath('//div[contains(@id,"game_name")]/h1/img/@src').extract()).split(u'icon')[-1].split(u'-')[0]+'.apk?downloadname='+name+'.apk'  # 下載
        # print apk_download
        # size = ''.join(sel.xpath('//div[contains(@class,"xx")]/text()').extract()).replace(u' ','').split(u'B')[0].replace(u'\r\n\t\xa0\xa0',u'')    # 大小
        # print size
        # version = ''.join(sel.xpath('//div[contains(@class,"xx")]/text()').extract()).split(u'|')[0].split(u'版本：')[-1]  # 版本
        # print version
        # download_number = ''.join(sel.xpath('//div[contains(@class,"xx")]/text()').extract()).split(u'次')[0].split(u'B')[-1].replace(u'\xa0\xa0',u'')  # 評分次數
        # print download_number
        # creator = 'zero'  # 開發者
        # print creator

        item = response.meta['item']
        item['project_id'] = self.project_id if self.project_id else 1
        item['apk_store'] = 'doyo'

        item['package_name'] = 'zero'
        item['name'] = 'zero'
        item['icon_url'] = 'zero'
        item['apk_download'] = 'zero'
        item['size'] = 'zero'
        item['version'] = 'zero'
        item['download_number'] = 'zero'
        item['creator'] = 'zero'

        # package_name(沒有)

        # 名稱
        name = ''.join(sel.xpath('//div[contains(@class,"gameName")]/h1/text()').extract())
        if name and name != '0':
            item['name'] = name  # 名稱

        # icon
        icon_url = ''.join(sel.xpath('//div[contains(@id,"gameBox")]/div[1]/img/@src').extract())
        if icon_url and icon_url != '0':
            item['icon_url'] = icon_url  # icon

        # 下載
        apk_download = 'http://apk.doyo.cn/mobile/apk'+''.join(sel.xpath('//div[contains(@id,"game_name")]/h1/img/@src').extract()).split(u'icon')[-1].split(u'-')[0]+'.apk?downloadname='+name+'.apk'
        if apk_download and apk_download != '0':
            # apk_download = urllib2.urlopen(apk_download, None, 8).geturl()
            item['apk_download'] = apk_download  # 下載

        # 大小
        size = ''.join(sel.xpath('//div[contains(@class,"xx")]/text()').extract()).replace(u' ','').split(u'B')[0].replace(u'\r\n\t\xa0\xa0',u'')
        if size and size != '0':
            item['size'] = size  # 大小

        # 版本
        version = ''.join(sel.xpath('//div[contains(@class,"xx")]/text()').extract()).split(u'|')[0].split(u'版本：')[-1]
        if version and version != '0':
            item['version'] = version  # 版本

        # 評分次數
        download_number = ''.join(sel.xpath('//div[contains(@class,"xx")]/text()').extract()).split(u'次')[0].split(u'B')[-1].replace(u'\xa0\xa0',u'')
        if download_number and download_number != '0':
            item['download_number'] = download_number  # 版本

        # 開發者(沒有)

        return item

