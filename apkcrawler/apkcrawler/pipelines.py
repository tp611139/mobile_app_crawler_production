__author__ = 'allen chen, 2016.3.20'
# -*- coding: utf-8 -*-
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo , scrapy , urllib2 , os , urllib
from scrapy . conf import settings
from scrapy . exceptions import DropItem
from scrapy import log
from scrapy . pipelines . files import FileException , FilesPipeline
if 64 - 64: i11iIiiIii
class MongoDBPipeline ( object ) :
 def __init__ ( self ) :
  OO0o = pymongo . MongoClient ( settings [ 'MONGODB_URI' ] )
  Oo0Ooo = OO0o [ settings [ 'MONGODB_DB' ] ]
  self . collection = Oo0Ooo [ settings [ 'MONGODB_COLLECTION' ] ]
  if 85 - 85: OOO0O0O0ooooo % IIii1I . II1 - O00ooooo00
 def process_item ( self , item , spider ) :
  I1IiiI = True
  for IIi1IiiiI1Ii in item :
   if not IIi1IiiiI1Ii :
    I1IiiI = False
    raise DropItem ( "Missing {0}!" . format ( IIi1IiiiI1Ii ) )
  if I1IiiI :
   self . collection . insert ( dict ( item ) )
   log . msg ( "Question added to MongoDB database!" , level = log . DEBUG , spider = spider )
  return item
  if 39 - 39: O0 - ooOO00oOo % oOo0O0Ooo * Ooo00oOo00o . oOoO0oo0OOOo + iiiiIi11i
class HandleApkPipeline ( object ) :
 def process_item ( self , item , spider ) :
  Ii1I = os . path . join ( os . path . split ( item [ 'apk_path' ] ) [ 0 ] , item [ 'apk_id' ] + '.apk' )
  log . msg ( "ReName:%s" % Ii1I , level = log . INFO , spider = spider )
  os . rename ( item [ 'apk_path' ] , Ii1I )
  item [ 'apk_path' ] = Ii1I
  return item
  if 48 - 48: iII111i % IiII + I1Ii111 / ooOoO0o * o00O0oo
class PostPipeline ( object ) :
 def process_item ( self , item , spider ) :
  if 97 - 97: oO0o0ooO0 - IIII / O0oO - o0oO0
  oo00 = { }
  if 'apk_path' in dict ( item ) :
   try :
    if 88 - 88: oO0o0ooO0 . IiII % o0oO0
    ooO0oooOoO0 = { 'Version' : item [ 'version' ] , 'App_Name' : item [ 'name' ] , 'DL_NUM' : item [ 'download_number' ] , 'DL_URL' : str ( item [ 'apk_download' ] ) , 'AppStore' : item [ 'apk_store' ] , 'APK_Name' : item [ 'apk_id' ] , 'Project_id' : item [ 'project_id' ] , 'APK_Path' : item [ 'apk_path' ] }
    oo00 . update ( ( k , v . encode ( 'utf8' ) ) if type ( v ) == unicode else ( k , v ) for k , v in ooO0oooOoO0 . items ( ) )
    log . msg ( "Post Data [%s][%s] to Server [%s]" % ( oo00 [ 'APK_Name' ] . decode ( 'utf8' ) , item [ 'apk_path' ] , settings [ 'A4P_HOST_URL' ] ) , level = log . INFO , spider = spider )
    urllib2 . urlopen ( url = settings [ 'A4P_HOST_URL' ] , data = urllib . urlencode ( oo00 ) , timeout = 30 )
   except Exception as II11i :
    log . msg ( "API Post Error %s" % ( str ( II11i ) ) , level = log . WARNING , spider = spider )
    print oo00
    log . msg ( "Post Format %s" % ( str ( oo00 ) ) , level = log . WARNING , spider = spider )
  return item
  if 43 - 43: o00O0oo . IiII
class APKFilesPipeline ( FilesPipeline ) :
 def get_media_requests ( self , item , info ) :
  IIii11I1 = ''
  if 'www.wandoujia.com' in item [ 'apk_download' ] :
   try : IIii11I1 = urllib2 . urlopen ( item [ 'apk_download' ] , None , 8 ) . geturl ( )
   except : pass
  else : IIii11I1 = item [ 'apk_download' ]
  if IIii11I1 : yield scrapy . Request ( url = IIii11I1 , meta = { "item" : item } )
  if 59 - 59: IIii1I
 def item_completed ( self , results , item , info ) :
  I11iii11IIi = [ O00o0o0000o0o [ 'path' ] for O0Oo , O00o0o0000o0o in results if O0Oo ]
  if not I11iii11IIi : raise DropItem ( "Item contains no file. %s" % item [ 'apk_download' ] )
  item [ 'apk_path' ] = os . path . join ( settings [ 'FILES_STORE' ] , '' . join ( I11iii11IIi ) )
  item [ 'apk_id' ] = results [ 0 ] [ 1 ] [ 'checksum' ]
  return item
  if 80 - 80: II1 . ooOO00oOo
 def file_path ( self , request , response = None , info = None ) :
  OOO0O = request . meta [ 'item' ]
  oo0ooO0oOOOOo = u'{0[apk_store]}/{0[package_name]}_{0[version]}.apk' . format ( OOO0O ) if 'package_name' in OOO0O else u'{0[apk_store]}/{0[name]}_{0[version]}.apk' . format ( OOO0O )
  return oo0ooO0oOOOOo
  if 71 - 71: O0oO
# dd678faae9ac167bc83abf78e5cb2f3f0688d3a3
