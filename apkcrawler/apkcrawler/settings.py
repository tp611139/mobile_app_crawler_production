# -*- coding: utf-8 -*-
# Scrapy settings for apkcrawler project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
BOT_NAME = 'apkcrawler'
SPIDER_MODULES = ['apkcrawler.spiders']
NEWSPIDER_MODULE = 'apkcrawler.spiders'
ITEM_PIPELINES =  {
    'apkcrawler.scrapy_redis.pipelines.RedisPipeline': 10,
    'apkcrawler.pipelines.APKFilesPipeline': 21,
    'apkcrawler.pipelines.HandleApkPipeline':801,
    'apkcrawler.pipelines.MongoDBPipeline':802,
    'apkcrawler.pipelines.PostPipeline': 803,
}
LOG_LEVEL = 'WARNING'#WARNING,INFO,DEBUG
MONGODB_URI = "mongodb://crawler:crawler@127.0.0.1:27017"
MONGODB_DB = "APK_Crawler"
MONGODB_COLLECTION = "AppStore"
FILES_STORE = '/var/appstore'
A4P_HOST_URL = 'http://192.168.0.204:13579/API/GNAUpload'

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY=2
# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN=2
#CONCURRENT_REQUESTS_PER_IP=16

# Disable cookies (enabled by default)
COOKIES_ENABLED=False
DOWNLOAD_WARNSIZE = 83886080
RETRY_TIMES = 3
RETRY_HTTP_CODES = [500, 502, 503, 504, 400, 408]
REFERER_ENABLED = True
# Disable Telnet Console (enabled by default)
TELNETCONSOLE_ENABLED=False

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'apkcrawler.middlewares.useragent.RotateUserAgentMiddleware': 9,
}

# Enables scheduling storing requests queue in redis.
SCHEDULER = "apkcrawler.scrapy_redis.scheduler.Scheduler"
# Don't cleanup redis queues, allows to pause/resume crawls.
SCHEDULER_PERSIST = True
# Schedule requests using a priority queue. (default)
SCHEDULER_QUEUE_CLASS = 'apkcrawler.scrapy_redis.queue.SpiderPriorityQueue'
SCHEDULER_IDLE_BEFORE_CLOSE = 10
# Store scraped item in redis for post-processing.
# Specify the host and port to use when connecting to Redis (optional).
REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379


DOWNLOAD_TIMEOUT=600