# -*- coding: utf-8 -*-
__author__ = 'allen chen, 20160.3.20'


# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class ApkcrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    icon_url = scrapy.Field()
    #APK中文名稱
    name = scrapy.Field()           #<---need
    package_name = scrapy.Field()   #<---need
    #APK下載URL
    apk_download = scrapy.Field()
    #下載次數 / 評分次數
    download_number = scrapy.Field()#<---need
    #APK版本
    version = scrapy.Field()        #<---need
    #檔案大小
    size = scrapy.Field()           #<---need
    #APK package ID
    apk_id = scrapy.Field()
    apk_path = scrapy.Field()
    project_id = scrapy.Field()     #<---need
    #AppStore 名稱
    apk_store = scrapy.Field()
    #評分
    rate = scrapy.Field()
    #開發者
    creator = scrapy.Field()
    #apk_scorefrequency = scrapy.Field()
    #date = scrapy.Field()
    #support_firmware = scrapy.Field()
    pass
