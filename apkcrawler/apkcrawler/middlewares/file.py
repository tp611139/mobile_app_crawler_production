#!/usr/bin/python
#-*-coding:utf-8-*-
__author__ = 'allen chen'
import os
import time
import hashlib
import urlparse
import shutil
import urllib
from urlparse import urlparse
from scrapy import log
from twisted . internet import defer
from pprint import pprint
from apkcrawler . misc . utils import color
from scrapy . utils . misc import md5sum
from collections import defaultdict
from scrapy . utils . misc import arg_to_iter
from scrapy . contrib . pipeline . images import MediaPipeline
from apkcrawler . misc . utils . utils . select_result import list_first_item
from scrapy . exceptions import NotConfigured , IgnoreRequest
if 64 - 64: i11iIiiIii
class FileException ( Exception ) :
 def __init__ ( self , file_url = None , * args ) :
  if 65 - 65: O0 / iIii1I11I1II1 % OoooooooOO - i1IIi
  self . file_url = file_url
  self . style = color . color_style ( )
  Exception . __init__ ( self , * args )
  if 73 - 73: II111iiii
 def __str__ ( self ) :
  print self . style . ERROR ( "ERROR(FileException): %s" % ( Exception . __str__ ( self ) , ) )
  if 22 - 22: I1IiiI * Oo0Ooo / OoO0O00 . OoOoOO00 . o0oOOo0O0Ooo / I1ii11iIi11i
  return Exception . __str__ ( self )
  if 48 - 48: oO0o / OOooOOo / I11i / Ii1I
class FSFilesStore ( object ) :
 if 48 - 48: iII111i % IiII + I1Ii111 / ooOoO0o * Ii1I
 def __init__ ( self , basedir ) :
  if '://' in basedir :
   basedir = basedir . split ( '://' , 1 ) [ 1 ]
  self . basedir = basedir
  self . _mkdir ( self . basedir )
  self . created_directories = defaultdict ( set )
  if 46 - 46: ooOoO0o * I11i - OoooooooOO
 def persist_file ( self , key , file_content , info , filename ) :
  self . _mkdir ( os . path . join ( self . basedir , * key . split ( '/' ) ) , info )
  II1iII1i = self . _get_filesystem_path ( key , filename )
  with open ( II1iII1i , "w" ) as oO0oIIII :
   oO0oIIII . write ( file_content )
   if 59 - 59: i1IIi * i1IIi % OOooOOo + II111iiii
  with open ( II1iII1i , 'rb' ) as file_content :
   II = md5sum ( file_content )
   if 100 - 100: i1IIi . I1Ii111 / IiII * OoooooooOO + I11i * oO0o
  return II
  if 99 - 99: iII111i . OOooOOo / iIii1I11I1II1 * iIii1I11I1II1
 def stat_file ( self , key , info ) :
  if 11 - 11: oO0o / i1IIi % II111iiii - OoOoOO00
  if 91 - 91: OoO0O00 . i11iIiiIii / oO0o % I11i / OoO0O00 - i11iIiiIii
  if 8 - 8: o0oOOo0O0Ooo * I1ii11iIi11i * iIii1I11I1II1 . IiII / IiII % IiII
  if 22 - 22: Ii1I . IiII
  if 41 - 41: I1Ii111 . ooOoO0o * IiII % i11iIiiIii
  o000o0o00o0Oo = os . path . join ( self . basedir , * key . split ( '/' ) )
  oo = os . listdir ( o000o0o00o0Oo )
  if len ( oo ) != 1 :
   shutil . rmtree ( o000o0o00o0Oo , True )
   return { }
  else :
   IiII1I1i1i1ii = list_first_item ( oo )
   if 44 - 44: oO0o / Oo0Ooo - II111iiii - i11iIiiIii % I1Ii111
  II1iII1i = self . _get_filesystem_path ( key )
  try :
   O0OoOoo00o = os . path . getmtime ( II1iII1i )
  except :
   return { }
   if 31 - 31: II111iiii + OoO0O00 . I1Ii111
  with open ( os . path . join ( II1iII1i , IiII1I1i1i1ii ) , 'rb' ) as OoOooOOOO :
   II = md5sum ( OoOooOOOO )
   if 45 - 45: I1Ii111 + Ii1I
  return { 'last_modified' : O0OoOoo00o , 'checksum' : II }
  if 17 - 17: o0oOOo0O0Ooo
 def _get_filesystem_path ( self , key , filename = None ) :
  o00ooooO0oO = key . split ( '/' )
  if filename :
   o00ooooO0oO . append ( filename )
   return os . path . join ( self . basedir , * o00ooooO0oO )
  else :
   return os . path . join ( self . basedir , * o00ooooO0oO )
   if 63 - 63: OoOoOO00 % i1IIi
 def _mkdir ( self , dirname , domain = None ) :
  if not os . path . exists ( dirname ) :
   os . makedirs ( dirname )
   if 66 - 66: Ii1I
   if 78 - 78: OoO0O00
   if 18 - 18: O0 - iII111i / iII111i + ooOoO0o % ooOoO0o - IiII
   if 62 - 62: iII111i - IiII - OoOoOO00 % i1IIi / oO0o
  dirname = dirname [ : dirname . rfind ( '/' ) ] if domain else dirname
  OoooooOoo = self . created_directories [ domain ] if domain else set ( )
  if dirname not in OoooooOoo :
   OoooooOoo . add ( dirname )
   if 70 - 70: OoO0O00 . OoO0O00 - OoO0O00 / I1ii11iIi11i * OOooOOo
class FilePipeline ( MediaPipeline ) :
 if 86 - 86: i11iIiiIii + Ii1I + ooOoO0o * I11i + o0oOOo0O0Ooo
 if 61 - 61: OoO0O00 / i11iIiiIii
 if 34 - 34: OoooooooOO + iIii1I11I1II1 + i11iIiiIii - I1ii11iIi11i + i11iIiiIii
 if 65 - 65: OoOoOO00
 MEDIA_NAME = 'file'
 EXPIRES = 90
 URL_GBK_DOMAIN = [ ]
 ATTACHMENT_FILENAME_UTF8_DOMAIN = [ ]
 STORE_SCHEMES = {
 '' : FSFilesStore ,
 'file' : FSFilesStore ,
 }
 if 6 - 6: I1IiiI / Oo0Ooo % Ii1I
 FILE_EXTENTION = [ '.doc' , '.txt' , '.docx' , '.rar' , '.zip' , '.pdf' ]
 if 84 - 84: i11iIiiIii . o0oOOo0O0Ooo
 def __init__ ( self , store_uri , download_func = None ) :
  if not store_uri :
   raise NotConfigured
  self . store = self . _get_store ( store_uri )
  self . style = color . color_style ( )
  super ( FilePipeline , self ) . __init__ ( download_func = download_func )
  if 100 - 100: Ii1I - Ii1I - I1Ii111
 @ classmethod
 def from_settings ( cls , settings ) :
  cls . EXPIRES = settings . getint ( 'FILE_EXPIRES' , 90 )
  cls . ATTACHMENT_FILENAME_UTF8_DOMAIN = settings . get ( 'ATTACHMENT_FILENAME_UTF8_DOMAIN' , [ ] )
  cls . URL_GBK_DOMAIN = settings . get ( 'URL_GBK_DOMAIN' , [ ] )
  cls . FILE_EXTENTION = settings . get ( 'FILE_EXTENTION' , [ ] )
  ii1 = settings [ 'FILE_STORE' ]
  return cls ( ii1 )
  if 57 - 57: Ii1I % OoooooooOO
 def _get_store ( self , uri ) :
  if os . path . isabs ( uri ) :
   O00 = 'file'
  else :
   O00 = urlparse . urlparse ( uri ) . scheme
   if 11 - 11: I1IiiI
  O0o0Oo = self . STORE_SCHEMES [ O00 ]
  return O0o0Oo ( uri )
  if 78 - 78: iIii1I11I1II1 - Ii1I * OoO0O00 + o0oOOo0O0Ooo + iII111i + iII111i
 def media_downloaded ( self , response , request , info ) :
  if 11 - 11: iII111i - OoO0O00 % ooOoO0o % iII111i / OoOoOO00 - OoO0O00
  if 74 - 74: iII111i * O0
  if 89 - 89: oO0o + Oo0Ooo
  if 3 - 3: i1IIi / I1IiiI % I11i * i11iIiiIii / O0 * I11i
  III1ii1iII = request . headers . get ( 'Referer' )
  if 54 - 54: I1IiiI % II111iiii % II111iiii
  if response . status != 200 :
   log . msg ( format = '%(medianame)s (code: %(status)s): Error downloading %(medianame)s from %(request)s referred in <%(referer)s>' ,
 level = log . WARNING , spider = info . spider , medianame = self . MEDIA_NAME ,
 status = response . status , request = request , referer = III1ii1iII )
   raise FileException ( request . url , '%s: download-error' % ( request . url , ) )
   if 13 - 13: o0oOOo0O0Ooo . Ii1I
  if not response . body :
   log . msg ( format = '%(medianame)s (empty-content): Empty %(medianame)s from %(request)s referred in <%(referer)s>: no-content' ,
 level = log . WARNING , spider = info . spider , medianame = self . MEDIA_NAME ,
 request = request , referer = III1ii1iII )
   raise FileException ( request . url , '%s: empty-content' % ( request . url , ) )
   if 19 - 19: I11i + ooOoO0o
  ooo = 'cached' if 'cached' in response . flags else 'downloaded'
  log . msg ( format = '%(medianame)s (%(status)s): Downloaded %(medianame)s from %(request)s referred in <%(referer)s>' ,
 level = log . DEBUG , spider = info . spider , medianame = self . MEDIA_NAME ,
 status = ooo , request = request , referer = III1ii1iII )
  if 18 - 18: o0oOOo0O0Ooo
  if self . is_valid_content_type ( response ) :
   raise FileException ( request . url , '%s: invalid-content_type' % ( request . url , ) )
   if 28 - 28: OOooOOo - IiII . IiII + OoOoOO00 - OoooooooOO + O0
  IiII1I1i1i1ii = self . get_file_name ( request , response )
  if 95 - 95: OoO0O00 % oO0o . O0
  if not IiII1I1i1i1ii :
   raise FileException ( request . url , '%s: noaccess-filename' % ( request . url , ) )
   if 15 - 15: ooOoO0o / Ii1I . Ii1I - i1IIi
  self . inc_stats ( info . spider , ooo )
  if 53 - 53: IiII + I1IiiI * oO0o
  try :
   OooOooooOOoo0 = self . file_key ( request . url )
   II = self . store . persist_file ( OooOooooOOoo0 , response . body , info , IiII1I1i1i1ii )
  except FileException as o00OO0OOO0 :
   oo0 = '%(medianame)s (error): Error processing %(medianame)s from %(request)s referred in <%(referer)s>: %(errormsg)s'
   log . msg ( format = oo0 , level = log . WARNING , spider = info . spider , medianame = self . MEDIA_NAME ,
 request = request , referer = III1ii1iII , errormsg = str ( o00OO0OOO0 ) )
   raise
   if 57 - 57: OOooOOo . OOooOOo
  return { 'url' : request . url , 'path' : OooOooooOOoo0 , 'checksum' : II }
  if 95 - 95: O0 + OoO0O00 . II111iiii / O0
 def media_failed ( self , failure , request , info ) :
  if not isinstance ( failure . value , IgnoreRequest ) :
   III1ii1iII = request . headers . get ( 'Referer' )
   log . msg ( format = '%(medianame)s (unknown-error): Error downloading '
 '%(medianame)s from %(request)s referred in '
 '<%(referer)s>: %(exception)s' ,
 level = log . WARNING , spider = info . spider , exception = failure . value ,
 medianame = self . MEDIA_NAME , request = request , referer = III1ii1iII )
   if 97 - 97: ooOoO0o - OOooOOo * i11iIiiIii / OoOoOO00 % I1Ii111 - OoooooooOO
  raise FileException ( request . url , '%s: Error downloading' % ( request . url , ) )
  if 59 - 59: O0 + I1IiiI + IiII % I1IiiI
 def media_to_download ( self , request , info ) :
  def o0OOoo0OO0OOO ( result ) :
   if 19 - 19: oO0o % i1IIi % o0oOOo0O0Ooo
   if not result :
    return
    if 93 - 93: iIii1I11I1II1 % oO0o * i1IIi
   O0OoOoo00o = result . get ( 'last_modified' , None )
   if not O0OoOoo00o :
    return
    if 16 - 16: O0 - I1Ii111 * iIii1I11I1II1 + iII111i
   Ii11iII1 = time . time ( ) - O0OoOoo00o
   Oo0O0O0ooO0O = Ii11iII1 / 60 / 60 / 24
   if Oo0O0O0ooO0O > self . EXPIRES :
    return
    if 15 - 15: I1ii11iIi11i + OoOoOO00 - OoooooooOO / OOooOOo
   III1ii1iII = request . headers . get ( 'Referer' )
   log . msg ( format = '%(medianame)s (uptodate): Downloaded %(medianame)s from %(request)s referred in <%(referer)s>' ,
 level = log . DEBUG , spider = info . spider ,
 medianame = self . MEDIA_NAME , request = request , referer = III1ii1iII )
   self . inc_stats ( info . spider , 'uptodate' )
   if 58 - 58: i11iIiiIii % I11i
   II = result . get ( 'checksum' , None )
   if 71 - 71: OOooOOo + ooOoO0o % i11iIiiIii + I1ii11iIi11i - IiII
   return { 'url' : request . url , 'path' : OooOooooOOoo0 , 'checksum' : II }
   if 88 - 88: OoOoOO00 - OoO0O00 % OOooOOo
  OooOooooOOoo0 = self . file_key ( request . url )
  iI1I111Ii111i = defer . maybeDeferred ( self . store . stat_file , OooOooooOOoo0 , info )
  iI1I111Ii111i . addCallbacks ( o0OOoo0OO0OOO , lambda I11IiI1I11i1i : None )
  iI1I111Ii111i . addErrback ( log . err , self . __class__ . __name__ + '.store.stat_file' )
  return iI1I111Ii111i
  if 38 - 38: o0oOOo0O0Ooo
 def is_valid_content_type ( self , response ) :
  return True
  if 57 - 57: O0 / oO0o * I1Ii111 / OoOoOO00 . II111iiii
  if 26 - 26: iII111i
  if 91 - 91: OoO0O00 . I1ii11iIi11i + OoO0O00 - iII111i / OoooooooOO
  if 39 - 39: I1ii11iIi11i / ooOoO0o - II111iiii
 def inc_stats ( self , spider , status ) :
  spider . crawler . stats . inc_value ( '%s_file_count' % ( self . MEDIA_NAME , ) , spider = spider )
  spider . crawler . stats . inc_value ( '%s_file_status_count/%s' % ( self . MEDIA_NAME , status ) , spider = spider )
  if 98 - 98: I1ii11iIi11i / I11i % oO0o . OoOoOO00
 def file_key ( self , url ) :
  if 91 - 91: oO0o % Oo0Ooo
  if 64 - 64: I11i % iII111i - I1Ii111 - oO0o
  if 31 - 31: I11i - II111iiii . I11i
  if 18 - 18: o0oOOo0O0Ooo
  O0o0O00Oo0o0 = hashlib . sha1 ( url ) . hexdigest ( )
  return '%s/%s' % ( urlparse ( url ) . netloc , O0o0O00Oo0o0 )
  if 87 - 87: ooOoO0o * Oo0Ooo % i11iIiiIii % OoOoOO00 - OOooOOo
 def get_file_name ( self , request , response ) :
  if 68 - 68: I1Ii111 % i1IIi . IiII . I1ii11iIi11i
  if 92 - 92: iII111i . I1Ii111
  if 31 - 31: I1Ii111 . OoOoOO00 / O0
  if 89 - 89: OoOoOO00
  if 68 - 68: OoO0O00 * OoooooooOO % O0 + OoO0O00 + ooOoO0o
  if 4 - 4: ooOoO0o + O0 * OOooOOo
  OOoo0O = response . headers . get ( 'Content-Disposition' , '' )
  IiII1I1i1i1ii = ""
  if 67 - 67: i11iIiiIii - i1IIi % I1ii11iIi11i . O0
  if OOoo0O :
   for o0oo in OOoo0O . split ( ';' ) :
    if "filename" in o0oo :
     if 91 - 91: IiII
     IiII1I1i1i1ii = o0oo . split ( 'filename=' ) [ 1 ] . strip ( " \n\'\"" )
     break
     if 15 - 15: II111iiii
  if IiII1I1i1i1ii :
   if 18 - 18: i11iIiiIii . i1IIi % OoooooooOO / O0
   if 75 - 75: OoOoOO00 % o0oOOo0O0Ooo % o0oOOo0O0Ooo . I1Ii111
   if 5 - 5: o0oOOo0O0Ooo * ooOoO0o + OoOoOO00 . OOooOOo + OoOoOO00
   if urlparse ( request . url ) . netloc in self . ATTACHMENT_FILENAME_UTF8_DOMAIN :
    IiII1I1i1i1ii = IiII1I1i1i1ii . decode ( "utf-8" )
   else :
    IiII1I1i1i1ii = IiII1I1i1i1ii . decode ( "gbk" )
    if 91 - 91: O0
  else :
   oOOo0 = request . url . split ( '/' ) [ - 1 ]
   if 54 - 54: O0 - IiII % OOooOOo
   if 77 - 77: OoOoOO00 / I1IiiI / OoO0O00 + OoO0O00 . OOooOOo
   if os . path . splitext ( oOOo0 ) [ 1 ] . lower ( ) in self . FILE_EXTENTION :
    if urlparse ( request . url ) . netloc in self . URL_GBK_DOMAIN :
     IiII1I1i1i1ii = urllib . unquote ( oOOo0 ) . decode ( "gbk" ) . encode ( "utf-8" )
    else :
     IiII1I1i1i1ii = urllib . unquote ( oOOo0 )
     if 38 - 38: I1Ii111
     if 7 - 7: O0 . iII111i % I1ii11iIi11i - I1IiiI - iIii1I11I1II1
  return IiII1I1i1i1ii
# dd678faae9ac167bc83abf78e5cb2f3f0688d3a3
